package com.tcs.vikrela.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.tcs.vikrela.R;
import com.tcs.vikrela.dto.PendingDTO;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by Harsh on 6/19/2017.
 *//*
public class RentRecordsAdapter extends RecyclerView.Adapter<RentRecordsAdapter.RentRecordsViewHolder> {

    private LayoutInflater inflater;
    private List<PendingDTO> pendingDTOList;
    Context context;

    public RentRecordsAdapter(Context context, List<PendingDTO> pendingDTOList, OnItemClickListener listener) {
        this.context = context;
        this.pendingDTOList = pendingDTOList;
        this.listener = listener;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public RentRecordsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.fragment_pending_custom_row, parent, false);
        RentRecordsViewHolder holder = new RentRecordsViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(RentRecordsViewHolder holder, int position) {
        holder.bind(pendingDTOList.get(position), listener);
    }

    @Override
    public int getItemCount() {
        return pendingDTOList.size();
    }

    public interface OnItemClickListener {
        void onItemClick(PendingDTO pendingDTO);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    private OnItemClickListener listener;

    class RentRecordsViewHolder extends RecyclerView.ViewHolder {
        TextView txtName, txtAddress, txtAmount;

        public RentRecordsViewHolder(View itemView) {
            super(itemView);
            txtName = (TextView) itemView.findViewById(R.id.txtName);
            txtAddress = (TextView) itemView.findViewById(R.id.txtAddress);
            txtAmount = (TextView) itemView.findViewById(R.id.txtAmount);
        }

        public void bind(final PendingDTO pendingDTO, final OnItemClickListener listener) {
            txtName.setText(pendingDTO.getName());
            txtAddress.setText(pendingDTO.getAddress());
            txtAmount.setText(pendingDTO.getAmount());
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(pendingDTO);
                }
            });
        }
    }
}*/
public class RentRecordsAdapter extends BaseAdapter{

    Context context;
    private List<PendingDTO> pendingDTOList;
    private LayoutInflater inflater;
    private ArrayList<PendingDTO> arrayList;

    public RentRecordsAdapter(Context context, List<PendingDTO> pendingDTOList) {
        this.context = context;
        this.pendingDTOList = pendingDTOList;
        this.arrayList = new ArrayList<PendingDTO>();
        this.arrayList.addAll(pendingDTOList);
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return pendingDTOList.size();
    }

    @Override
    public Object getItem(int position) {
        return pendingDTOList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView txtReportName, txtReportAddress, txtViolation;
        if (convertView == null){
            convertView = inflater.inflate(R.layout.report_custom_row, parent, false);
            txtReportName = (TextView) convertView.findViewById(R.id.txtReportName);
            txtReportAddress = (TextView) convertView.findViewById(R.id.txtReportAddress);
            txtViolation = (TextView) convertView.findViewById(R.id.txtViolation);
            txtReportName.setText(pendingDTOList.get(position).getName());
            txtReportAddress.setText(pendingDTOList.get(position).getAddress());
            txtViolation.setText(pendingDTOList.get(position).getAmount());
        }
        return convertView;
    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        pendingDTOList.clear();
        if (charText.length() == 0) {
            pendingDTOList.addAll(arrayList);
        } else {
            for (PendingDTO dto : arrayList) {
                if (dto.getId().toLowerCase(Locale.getDefault()).contains(charText)) {
                    pendingDTOList.add(dto);
                }
            }
        }
        notifyDataSetChanged();
    }
}
