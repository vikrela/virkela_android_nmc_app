package com.tcs.vikrela.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tcs.vikrela.R;
import com.tcs.vikrela.dto.CollectedDTO;

import java.util.List;

/**
 * Created by Harsh on 6/11/2017.
 */
public class CollectedAdapter extends RecyclerView.Adapter<CollectedAdapter.CollectedViewHolder> {

    Context context;
    private List<CollectedDTO> collectedDTOList;
    private LayoutInflater inflater;
    private OnItemClickListener listener;

    public CollectedAdapter(Context context, List<CollectedDTO> collectedDTOList, OnItemClickListener listener) {
        this.context = context;
        this.collectedDTOList = collectedDTOList;
        this.listener = listener;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public CollectedViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.fragment_pending_custom_row, parent, false);
        CollectedViewHolder holder = new CollectedViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(CollectedViewHolder holder, int position) {
        holder.bind(collectedDTOList.get(position), listener);
    }

    @Override
    public int getItemCount() {
        return collectedDTOList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public interface OnItemClickListener {
        void onItemClick(CollectedDTO collectedDTO);
    }

    class CollectedViewHolder extends RecyclerView.ViewHolder {

        TextView txtName, txtAddress, txtAmount;

        public CollectedViewHolder(View itemView) {
            super(itemView);

            txtName = (TextView) itemView.findViewById(R.id.txtName);
            txtAddress = (TextView) itemView.findViewById(R.id.txtAddress);
            txtAmount = (TextView) itemView.findViewById(R.id.txtAmount);
        }

        public void bind(final CollectedDTO collectedDTO, final OnItemClickListener listener) {
            txtName.setText(collectedDTO.getName());
            txtAddress.setText(collectedDTO.getAddress());
            txtAmount.setText(collectedDTO.getAmount());
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(collectedDTO);
                }
            });
        }
    }
}
