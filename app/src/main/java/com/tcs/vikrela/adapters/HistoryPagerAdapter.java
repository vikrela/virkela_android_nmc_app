package com.tcs.vikrela.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.tcs.vikrela.fragments.CollectedFragment;
import com.tcs.vikrela.fragments.PendingFragment;

/**
 * Created by Harsh on 6/8/2017.
 */
public class HistoryPagerAdapter extends FragmentStatePagerAdapter {
    public HistoryPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position) {
            case 0:
                fragment = new CollectedFragment();
                break;
            case 1:
                fragment = new PendingFragment();
                break;
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Rent";
            case 1:
                return "Inspection";
            default:
                return null;
        }

    }
}
