package com.tcs.vikrela.adapters;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tcs.vikrela.R;
import com.tcs.vikrela.dto.PendingDTO;

import java.util.List;

import xyz.danoz.recyclerviewfastscroller.vertical.VerticalRecyclerViewFastScroller;

/**
 * Created by Harsh on 6/8/2017.
 */
public class PendingAdapter extends RecyclerView.Adapter<PendingAdapter.PendingViewHolder> {

    /*
        private LayoutInflater inflater;
        private List<PendingDTO> pendingDTOList;
        private Context context;
        private final int VIEW_TYPE_ITEM = 0;
        private final int VIEW_TYPE_LOADING = 1;

        public interface OnLoadMoreListener {
            void onLoadMore();
        }

        private OnLoadMoreListener onLoadMoreListener;

        public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
            this.onLoadMoreListener = mOnLoadMoreListener;
        }

        private boolean isLoading;
        private int visibleThreshold = 10;
        private int lastVisibleItem, totalItemCount;

        public PendingAdapter(RecyclerView recyclerView, Context context, List<PendingDTO> pendingDTOList, OnItemClickListener listener) {
            this.context = context;
            this.pendingDTOList = pendingDTOList;
            this.listener = listener;
            inflater = LayoutInflater.from(context);

            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                    if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        if (onLoadMoreListener != null) {
                            onLoadMoreListener.onLoadMore();
                        }
                        isLoading = true;
                    }
                }
            });
        }

        @Override
        public int getItemViewType(int position) {
            return pendingDTOList.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            if (viewType == VIEW_TYPE_ITEM) {
                View view = inflater.inflate(R.layout.fragment_pending_custom_row, parent, false);
                PendingViewHolder holder1 = new PendingViewHolder(view);
                return holder1;
            } else if (viewType == VIEW_TYPE_LOADING) {
                View view = inflater.inflate(R.layout.item_loading, parent, false);
                LoadingViewHolder holder = new LoadingViewHolder(view);
                return holder;
            }
            return null;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            if (holder instanceof PendingViewHolder) {
                ((PendingViewHolder) holder).bind(pendingDTOList.get(position), listener);
            } else if (holder instanceof LoadingViewHolder) {
                LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
                loadingViewHolder.progressBar.setIndeterminate(true);
            }
        }

        @Override
        public int getItemCount() {
            return pendingDTOList == null ? 0 : pendingDTOList.size();
        }

        public void setLoaded() {
            isLoading = false;
        }

        public interface OnItemClickListener {
            void onItemClick(PendingDTO pendingDTO);
        }

        private OnItemClickListener listener;

        class PendingViewHolder extends RecyclerView.ViewHolder {

            TextView txtName, txtAddress, txtAmount;

            public PendingViewHolder(View itemView) {
                super(itemView);
                txtName = (TextView) itemView.findViewById(R.id.txtName);
                txtAddress = (TextView) itemView.findViewById(R.id.txtAddress);
                txtAmount = (TextView) itemView.findViewById(R.id.txtAmount);
            }

            public void bind(final PendingDTO pendingDTO, final OnItemClickListener listener) {
                txtName.setText(pendingDTO.getName());
                txtAddress.setText(pendingDTO.getAddress());
                txtAmount.setText(pendingDTO.getAmount());
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        listener.onItemClick(pendingDTO);
                    }
                });
            }
        }

        class LoadingViewHolder extends RecyclerView.ViewHolder {

            public ProgressBar progressBar;

            public LoadingViewHolder(View itemView) {
                super(itemView);
                progressBar = (ProgressBar) itemView.findViewById(R.id.progressBar);
            }
        }
    */
    private LayoutInflater inflater;
    private List<PendingDTO> pendingDTOList;
    Context context;

    public PendingAdapter(Context context, List<PendingDTO> pendingDTOList, OnItemClickListener listener) {
        this.context = context;
        this.pendingDTOList = pendingDTOList;
        this.listener = listener;
        //inflater = LayoutInflater.from(context);
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public PendingViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.fragment_pending_custom_row, parent, false);
        PendingViewHolder holder = new PendingViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(PendingViewHolder holder, int position) {
        holder.bind(pendingDTOList.get(position), listener);
    }

    @Override
    public int getItemCount() {
        return pendingDTOList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public interface OnItemClickListener {
        void onItemClick(PendingDTO pendingDTO);
    }

    private OnItemClickListener listener;

    class PendingViewHolder extends RecyclerView.ViewHolder {

        TextView txtName, txtAddress, txtAmount;

        public PendingViewHolder(View itemView) {
            super(itemView);
            txtName = (TextView) itemView.findViewById(R.id.txtName);
            txtAddress = (TextView) itemView.findViewById(R.id.txtAddress);
            txtAmount = (TextView) itemView.findViewById(R.id.txtAmount);
        }

        public void bind(final PendingDTO pendingDTO, final OnItemClickListener listener) {
            txtName.setText(pendingDTO.getName());
            txtAddress.setText(pendingDTO.getAddress());
            txtAmount.setText(pendingDTO.getAmount());
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(pendingDTO);
                }
            });
        }
    }
}
