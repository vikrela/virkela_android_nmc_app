package com.tcs.vikrela.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.tcs.vikrela.R;
import com.tcs.vikrela.dto.PendingDTO;
import com.tcs.vikrela.dto.ReportDTO;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by Harsh on 7/4/2017.
 */
public class InspectionReportAdapter extends BaseAdapter {
    Context context;
    private List<ReportDTO> reportDTOList;
    LayoutInflater inflater;
    ArrayList<ReportDTO> arrayList;

    public InspectionReportAdapter(Context context, List<ReportDTO> reportDTOList) {
        this.context = context;
        this.reportDTOList = reportDTOList;
        this.arrayList = new ArrayList<ReportDTO>();
        this.arrayList.addAll(reportDTOList);
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return reportDTOList.size();
    }

    @Override
    public Object getItem(int position) {
        return reportDTOList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView txtReportName, txtReportAddress, txtViolation;
        if (convertView == null) {
            //inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.report_custom_row, parent, false);
            txtReportName = (TextView) convertView.findViewById(R.id.txtReportName);
            txtReportAddress = (TextView) convertView.findViewById(R.id.txtReportAddress);
            txtViolation = (TextView) convertView.findViewById(R.id.txtViolation);
            txtReportName.setText(reportDTOList.get(position).getName());
            txtReportAddress.setText(reportDTOList.get(position).getAddress());
            txtViolation.setText(reportDTOList.get(position).getViolation());
            if (txtViolation.getText().equals("Enforcement")){
                txtViolation.setTextColor(Color.RED);
            }
            if (txtViolation.getText().toString().equals("Violation 1")){
                txtViolation.setTextColor(ContextCompat.getColor(context, R.color.mustard));
            }
            if (txtViolation.getText().toString().equals("Violation 2")){
                txtViolation.setTextColor(ContextCompat.getColor(context, R.color.orange));
            }
            if (txtViolation.getText().toString().equals("No Violation")){
                txtViolation.setTextColor(Color.GREEN);
            }
        }
        return convertView;
    }
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        reportDTOList.clear();
        if (charText.length() == 0) {
            reportDTOList.addAll(arrayList);
        } else {
            for (ReportDTO dto : arrayList) {
                if (dto.getId().toLowerCase(Locale.getDefault()).contains(charText)) {
                    reportDTOList.add(dto);
                }
            }
        }
        notifyDataSetChanged();
    }
}
