package com.tcs.vikrela.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tcs.vikrela.R;
import com.tcs.vikrela.dto.InspectionDTO;
import com.tcs.vikrela.dto.PendingDTO;

import java.util.List;

/**
 * Created by Harsh on 6/12/2017.
 */
public class InspectionAdapter extends RecyclerView.Adapter<InspectionAdapter.InspectionViewHolder> {

    private LayoutInflater inflater;
    private List<InspectionDTO> inspectionDTOList;
    Context context;

    public InspectionAdapter(Context context, List<InspectionDTO> inspectionDTOList, OnItemClickListener listener){
        this.context = context;
        this.inspectionDTOList = inspectionDTOList;
        this.listener = listener;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public InspectionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.fragment_inspection_custom_row, parent, false);
        InspectionViewHolder holder = new InspectionViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(InspectionViewHolder holder, int position) {
        holder.bind(inspectionDTOList.get(position), listener);
    }

    @Override
    public int getItemCount() {
        return inspectionDTOList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


    public interface OnItemClickListener {
        void onItemClick(InspectionDTO inspectionDTO);
    }

    private OnItemClickListener listener;

    class InspectionViewHolder extends RecyclerView.ViewHolder {

        TextView txtName, txtAddress, txtAmount;

        public InspectionViewHolder(View itemView) {
            super(itemView);
            txtName = (TextView) itemView.findViewById(R.id.txtName);
            txtAddress = (TextView) itemView.findViewById(R.id.txtAddress);
        }

        public void bind(final InspectionDTO inspectionDTO, final OnItemClickListener listener) {
            txtName.setText(inspectionDTO.getName());
            txtAddress.setText(inspectionDTO.getAddress());
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(inspectionDTO);
                }
            });
        }
    }
}
