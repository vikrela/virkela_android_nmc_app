package com.tcs.vikrela.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.tcs.vikrela.R;
import com.tcs.vikrela.constants.NetworkConstants;
import com.tcs.vikrela.global.AppController;
import com.tcs.vikrela.ui.Snackbar;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Harsh on 6/12/2017.
 */

// This activity is used for the inspection form with various ifs and buts applied as followed in the code.

public class InspectionActivity extends AppCompatActivity {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.txtSiteInfo)
    TextView txtSiteInfo;
    @Bind(R.id.radioStyle)
    RadioGroup radioGroupStyle;
    @Bind(R.id.radiobtnFixed)
    RadioButton radioBtnFixed;
    @Bind(R.id.radiobtnNotFixed)
    RadioButton radioBtnNotFixed;
    @Bind(R.id.spinnerBusinessType)
    Spinner spinnerBusinessType;
    @Bind(R.id.etArea)
    TextInputEditText etArea;
    @Bind(R.id.radioGroupQR)
    RadioGroup radioGroupQR;
    @Bind(R.id.radiobtnHasQR)
    RadioButton radiobtnHasQR;
    @Bind(R.id.radiobtnNoQR)
    RadioButton radiobtnNoQR;
    @Bind(R.id.llForms)
    LinearLayout llForms;
    @Bind(R.id.etCheckLicense)
    TextInputEditText etCheckLicense;
    @Bind(R.id.txtScanQR)
    TextView txtScanQR;
    @Bind(R.id.etLocation)
    TextInputEditText etLocation;
    @Bind(R.id.etName)
    TextInputEditText etName;
    @Bind(R.id.etMobile)
    TextInputEditText etMobile;
    @Bind(R.id.btnSubmitInspection)
    Button btnSubmitInspection;
    @Bind(R.id.txtHasQRText)
    TextView txtHasQRText;
    private Bitmap bitmap;
    String id;
    MaterialDialog dialog;
    SharedPreferences preferences;

    // the below variables are for the inspection checkpoints.
    String style = "";
    String urlId = "";
    String license = "";
    String hasQr = "";
    Bundle bundle;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inspection);
        preferences = getSharedPreferences("Vikrela", Context.MODE_PRIVATE);
        populate();
    }

    private void populate() {
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.inspection_form);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        etCheckLicense.setVisibility(View.GONE);
        etCheckLicense.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        etArea.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        // check whether the officer has scanned the Qr Code or not for inspection
        bundle = getIntent().getExtras();
        if (null != bundle) {
            if (bundle.containsKey("scanned") && bundle.get("scanned").equals("yes")) {
                txtHasQRText.setVisibility(View.GONE);
                radioGroupQR.setVisibility(View.GONE);
                txtScanQR.setVisibility(View.GONE);
                hasQr = "yes";
                urlId = preferences.getString("qr_code_id", null);
                license = "";
            }
            if (bundle.containsKey("sent_from_server") && bundle.get("sent_from_server").equals("yes")) {
                license = preferences.getString("license_number", null);
                //        etCheckLicense.setVisibility(View.GONE);
            }
        }

        // image for reviewing the site
        txtSiteInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, 1900);
            }
        });

        txtScanQR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(InspectionActivity.this, InspectScanQRActivity.class);
                startActivityForResult(intent, 0);
            }
        });

        // this radio group enables you to apply the given conditions for having and not having the qr code
        radioGroupQR.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (radiobtnNoQR.isChecked()) {
                    if (bundle.containsKey("sent_from_server") && bundle.getString("sent_from_server").equals("yes")) {
                        etCheckLicense.setVisibility(View.GONE);
                    } else {
                        etCheckLicense.setVisibility(View.VISIBLE);
                    }
                    txtScanQR.setVisibility(View.GONE);
                }
                if (radiobtnHasQR.isChecked()) {
                    etCheckLicense.setVisibility(View.GONE);
                    txtScanQR.setVisibility(View.VISIBLE);
                }
            }
        });

        btnSubmitInspection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog = new MaterialDialog.Builder(InspectionActivity.this)
                        .title(getString(R.string.dialog_title))
                        .content(getString(R.string.dialog_content))
                        .progress(true, 0)
                        .show();

                // below provides a better view of the conditions which are applied while filling out the inspection.
                if (!preferences.getString("has_qr_code", null).equals(null)) {
                    hasQr = preferences.getString("has_qr_code", null);
                }
                // this change of preference prevents error in has qr field while entering the license number for inspection
                preferences.edit().putString("has_qr_code", "yes").commit();
                if (radioBtnFixed.isChecked()) {
                    style = "fixed";
                } else {
                    style = "not fixed";
                }
                if (radiobtnHasQR.isChecked()) {
                    urlId = id;
                    hasQr = "yes";
                    license = "";
                } else if (radiobtnNoQR.isChecked()) {
                    hasQr = "no";
                    if (null != bundle){
                        if (bundle.containsKey("sent_from_server") && !bundle.getString("sent_from_server").equals("yes")) {
                            license = etCheckLicense.getText().toString();
                        }
                    }
                    urlId = "";
                }

                // Volley request for sending inspection data to the server

                StringRequest stringRequest = new StringRequest(Request.Method.POST, NetworkConstants.INSPECTION_URL, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialog.dismiss();
                        Log.i("Response:::", response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            boolean success = jsonObject.getBoolean("Success");
                            if (success) {
                                Intent intent = new Intent(InspectionActivity.this, NMCDashboardActivity.class);
                                startActivity(intent);
                                ActivityCompat.finishAffinity(InspectionActivity.this);
                            } else {
                                Snackbar.show(InspectionActivity.this, getString(R.string.error_message));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dialog.dismiss();
                        Snackbar.show(InspectionActivity.this, getString(R.string.network_error));
                        //Log.e("Error:::", error.toString());
                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        String nmc_id = preferences.getString("nmc_id", null);
                        //String id = preferences.getString("qr_code_id", null);
                        String image = getStringImage(bitmap);
                        params.put("image", image);
                        params.put("style", style);
                        params.put("productType", spinnerBusinessType.getSelectedItem().toString());
                        params.put("hasQrCode", hasQr);
                        params.put("lisenceNo", license);
                        params.put("area", etArea.getText().toString());
                        params.put("location", etLocation.getText().toString());
                        params.put("hawkerName", etName.getText().toString());
                        params.put("hawkerMobileNo", etMobile.getText().toString());
                        params.put("qrCode", urlId);
                        params.put("NMCId", nmc_id);
                        //params.put("qrCode", id);
                        return params;
                    }
                };
                AppController.getInstance().addToRequestQueue(stringRequest);
            }
        });
    }

    // Method to encode the image to string using base64 algorithm
    public String getStringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 50, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

    @Override
    protected void onActivityResult(int requestCode, final int resultCode, Intent data) {
        if (requestCode == 1900 && resultCode == RESULT_OK) {
            bitmap = (Bitmap) data.getExtras().get("data");
            txtSiteInfo.setText(R.string.image_saved);
            txtSiteInfo.setEnabled(false);
        }
        if (requestCode == 0 && resultCode == RESULT_OK) {
            String url = (String) data.getExtras().get("URL");
            if (!url.equals("")) {
                StringTokenizer str = new StringTokenizer(url, "id=");
                while (str.hasMoreElements()) {
                    id = str.nextElement().toString();
                }
                txtScanQR.setText("Hawker ID : " + id);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
