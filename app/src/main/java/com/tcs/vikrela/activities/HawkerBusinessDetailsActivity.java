package com.tcs.vikrela.activities;

import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.tcs.vikrela.R;
import com.tcs.vikrela.constants.NetworkConstants;
import com.tcs.vikrela.global.AppController;
import com.tcs.vikrela.ui.Snackbar;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Harsh on 6/7/2017.
 */

// This is also a form for filling the
public class HawkerBusinessDetailsActivity extends AppCompatActivity {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.layoutBusinessYears)
    TextInputLayout layoutBusinessYears;
    @Bind(R.id.etBusinessYears)
    TextInputEditText etBusinessYears;
    @Bind(R.id.txtTimeFrom)
    TextView txtTimeFrom;
    @Bind(R.id.txtTimeTo)
    TextView txtTimeTo;
    @Bind(R.id.spinnerBusinessType)
    Spinner spinnerBusinessType;
    @Bind(R.id.radioStyle)
    RadioGroup radioStyle;
    @Bind(R.id.radiobtnFixed)
    RadioButton radiobtnFixed;
    @Bind(R.id.radiobtnNotFixed)
    RadioButton radiobtnNotFixed;
    @Bind(R.id.btnProceedBusiness)
    Button btnProceedBusiness;
    TimePickerDialog timePickerDialog;
    Calendar calendar;
    String format;
    int calendarHour, calendarMinute;
    int hour1, hour2;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hawker_business_details);
        populate();
    }

    private void populate() {
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.fill_business_details);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // txtTimeTo and txtTimeFrom are use to open the material watch for setting up the duration of the hawking time for a particular hawker

        txtTimeFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calendar = Calendar.getInstance();
                calendarHour = calendar.get(Calendar.HOUR_OF_DAY);
                calendarMinute = calendar.get(Calendar.MINUTE);
                //the below method sets up the time like 12hrs time
                timePickerDialog = new TimePickerDialog(HawkerBusinessDetailsActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        if (hourOfDay == 0) {

                            hourOfDay += 12;

                            format = "AM";
                        } else if (hourOfDay == 12) {

                            format = "PM";

                        } else if (hourOfDay > 12) {

                            hourOfDay -= 12;

                            format = "PM";

                        } else {

                            format = "AM";
                        }
                        txtTimeFrom.setText(hourOfDay + " : " + minute + " " + format);
                        hour1 = view.getCurrentHour();
                    }
                }, calendarHour, calendarMinute, false);
                timePickerDialog.show();

            }
        });

        // similar to time timeFrom
        txtTimeTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calendar = Calendar.getInstance();
                calendarHour = calendar.get(Calendar.HOUR_OF_DAY);
                calendarMinute = calendar.get(Calendar.MINUTE);
                timePickerDialog = new TimePickerDialog(HawkerBusinessDetailsActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        if (hourOfDay == 0) {

                            hourOfDay += 12;

                            format = "AM";
                        } else if (hourOfDay == 12) {

                            format = "PM";

                        } else if (hourOfDay > 12) {

                            hourOfDay -= 12;

                            format = "PM";

                        } else {

                            format = "AM";
                        }

                        txtTimeTo.setText(hourOfDay + " : " + minute + " " + format);
                        hour2 = view.getCurrentHour();
                    }
                }, calendarHour, calendarMinute, false);
                timePickerDialog.show();
            }
        });

        btnProceedBusiness.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // validations applied for the checking the empty and invalid entries

                if (etBusinessYears.getText().toString().equals("")) {
                    Snackbar.show(HawkerBusinessDetailsActivity.this, getString(R.string.business_years_error));
                    etBusinessYears.requestFocus();
                    etBusinessYears.setError(getString(R.string.number_of_years_error));
                    return;
                }
                if (txtTimeFrom.getText().toString().equals(getString(R.string.time_from))) {
                    Snackbar.show(HawkerBusinessDetailsActivity.this, getString(R.string.time_error));
                    txtTimeFrom.requestFocus();
                    return;
                }
                if (txtTimeTo.getText().toString().equals(getString(R.string.time_to))) {
                    Snackbar.show(HawkerBusinessDetailsActivity.this, getString(R.string.time_error));
                    txtTimeTo.requestFocus();
                    return;
                }
                final MaterialDialog dialog = new MaterialDialog.Builder(HawkerBusinessDetailsActivity.this)
                        .title(getString(R.string.dialog_register_title))
                        .content(getString(R.string.dialog_message))
                        .progress(true, 0)
                        .cancelable(false)
                        .show();

                // Volley request sending the business details to the server

                StringRequest stringRequest = new StringRequest(Request.Method.POST, NetworkConstants.BUSINESS_DETAILS_URL, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("Response:::", response);
                        dialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int success = jsonObject.getInt("success");
                            if (success == 1) {
                                Intent intent = new Intent(HawkerBusinessDetailsActivity.this, HawkerNomineeDetailsActivity.class);
                                startActivity(intent);
                                Toast.makeText(getApplicationContext(), "Hawker Registered...", Toast.LENGTH_SHORT).show();
                                finish();
                            } else if (success == 0) {
                                Snackbar.show(HawkerBusinessDetailsActivity.this, getString(R.string.error_message));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("Error:::", error.toString());
                        dialog.dismiss();
                        Snackbar.show(HawkerBusinessDetailsActivity.this, getString(R.string.network_error));
                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        // applying validations and conditions to the params to be sent
                        String id, mobile = "";
                        Bundle bundle = getIntent().getExtras();
                        if (null != bundle) {
                            if (bundle.containsKey("id") && bundle.containsKey("mobile")) {
                                id = bundle.getString("id");
                                mobile = bundle.getString("mobile");
                            } else {
                                id = "";
                            }
                        } else {
                            id = "";
                        }
                        String style;
                        if (radiobtnFixed.isChecked()) {
                            style = "Fixed";
                        } else if (radiobtnNotFixed.isChecked()) {
                            style = "Not Fixed";
                        } else {
                            style = "";
                        }
                        params.put("id", id);
                        params.put("HawkerMobile", mobile);
                        params.put("Period", etBusinessYears.getText().toString());
                        params.put("TimeFrom", txtTimeFrom.getText().toString());
                        params.put("TimeTo", txtTimeTo.getText().toString());
                        params.put("BusinessType", spinnerBusinessType.getSelectedItem().toString());
                        params.put("Style", style);
                        return params;
                    }
                };
                stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                AppController.getInstance().addToRequestQueue(stringRequest);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
