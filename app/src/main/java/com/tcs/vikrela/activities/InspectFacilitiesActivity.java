package com.tcs.vikrela.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.tcs.vikrela.R;

import butterknife.Bind;
import butterknife.ButterKnife;

public class InspectFacilitiesActivity extends AppCompatActivity {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.spinnerFacilities)
    Spinner spinnerFacilities;
    @Bind(R.id.etIssueTitle)
    EditText etIssueTitle;
    @Bind(R.id.etIssueDescription)
    EditText etIssueDescription;
    @Bind(R.id.btnSubmit)
    Button btnSubmit;
    @Bind(R.id.txtSitePhoto)
    TextView txtSitePhoto;
    Bitmap bitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inspect_facilities);
        populate();
    }

    private void populate() {
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle("Inspect Facilities");

        txtSitePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, 1900);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, final int resultCode, Intent data) {
        if (requestCode == 1900 && resultCode == RESULT_OK) {
            bitmap = (Bitmap) data.getExtras().get("data");
            txtSitePhoto.setText(R.string.image_saved);
            txtSitePhoto.setEnabled(false);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
