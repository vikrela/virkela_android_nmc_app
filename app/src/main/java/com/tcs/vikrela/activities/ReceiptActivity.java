package com.tcs.vikrela.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.webkit.WebView;

import com.tcs.vikrela.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Harsh on 6/11/2017.
 */
public class ReceiptActivity extends AppCompatActivity {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.webViewReceipt)
    WebView webViewReceipt;
    public static final String LEGAL_URL = "https://view.publitas.com/digital-impact-square/rent-receipt_legal/";
    public static final String ILLEGAL_URL = "https://view.publitas.com/digital-impact-square/rent-receipt_illegal/";


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receipt);
        populate();
    }

    private void populate() {
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.payment_receipt);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        webViewReceipt.getSettings().setSupportZoom(true);
        webViewReceipt.getSettings().setJavaScriptEnabled(true);
        webViewReceipt.loadUrl(LEGAL_URL);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                startActivity(new Intent(this, NMCDashboardActivity.class));
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
