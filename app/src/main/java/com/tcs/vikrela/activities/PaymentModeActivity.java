package com.tcs.vikrela.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.tcs.vikrela.R;
import com.tcs.vikrela.constants.NetworkConstants;
import com.tcs.vikrela.global.AppController;
import com.tcs.vikrela.ui.Snackbar;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Harsh on 6/8/2017.
 */
// Allows you to switch between the different modes of payment i.e. auto debit and cash
public class PaymentModeActivity extends AppCompatActivity {
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.radiogroupPayment)
    RadioGroup radioGroupPayment;
    @Bind(R.id.radioAutoDebit)
    RadioButton radioAutoDebit;
    @Bind(R.id.txtDebitInfo)
    TextView txtDebitInfo;
    @Bind(R.id.radioCash)
    RadioButton radioCash;
    @Bind(R.id.btnSubmitPaymentMode)
    Button btnProceedPayment;
    SharedPreferences preferences;

    @OnClick(R.id.btnSubmitPaymentMode)
    void otp() {
        /*startActivity(new Intent(this, OtpActivity.class));
        finish();*/
        getOtp();

    }

    private void getOtp() {
        final MaterialDialog dialog = new MaterialDialog.Builder(PaymentModeActivity.this)
                .title("Please Wait")
                .content("Sending OTP...")
                .progress(true, 0)
                .show();
        StringRequest request = new StringRequest(Request.Method.POST, NetworkConstants.GET_OTP_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                dialog.dismiss();
                Log.d("Response:::", response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    boolean success = jsonObject.getBoolean("success");
                    if (success) {
                        startActivity(new Intent(PaymentModeActivity.this, OtpActivity.class));
                        finish();
                    } else {
                        Snackbar.show(PaymentModeActivity.this, "Some Error Occurred!!!");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Snackbar.show(PaymentModeActivity.this, "Some Error Occurred!!!");
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                Log.e("Error:::", error.toString());
                Snackbar.show(PaymentModeActivity.this, getString(R.string.network_error));
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("QrCodeId", preferences.getString("hawker_id", null));
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(request);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_mode);
        preferences = getSharedPreferences("Vikrela", Context.MODE_PRIVATE);
        populate();
    }

    private void populate() {
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.choose_payment_mode);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        radioAutoDebit.setEnabled(false);
        txtDebitInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(PaymentModeActivity.this, HawkerBankDetailsActivity.class));
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
