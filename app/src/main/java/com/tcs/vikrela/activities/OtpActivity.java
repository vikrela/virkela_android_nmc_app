package com.tcs.vikrela.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.tcs.vikrela.R;
import com.tcs.vikrela.constants.NetworkConstants;
import com.tcs.vikrela.global.AppController;
import com.tcs.vikrela.ui.Snackbar;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Harsh on 6/11/2017.
 */

// This activity is self explanatory in accordance with the name
public class OtpActivity extends AppCompatActivity {

    @Bind(R.id.btnSubmitOtp)
    Button btnSubmitOtp;
    @Bind(R.id.etOtp)
    EditText etOtp;
    SharedPreferences preferences;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);
        ButterKnife.bind(this);
        preferences = getSharedPreferences("Vikrela", Context.MODE_PRIVATE);
        btnSubmitOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etOtp.getText().toString().equals("")) {
                    Snackbar.show(OtpActivity.this, getString(R.string.empty_field_error));
                    return;
                }
                if (etOtp.getText().toString().length() < 6) {
                    Snackbar.show(OtpActivity.this, "Invalid OTP!!!");
                    return;
                }
                final MaterialDialog dialog = new MaterialDialog.Builder(OtpActivity.this)
                        .title(getString(R.string.dialog_title))
                        .content(getString(R.string.dialog_content))
                        .progress(true, 0)
                        .show();
                StringRequest request = new StringRequest(Request.Method.POST, NetworkConstants.PAY_RENT_URL, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialog.dismiss();
                        Log.i("Response::::", response);

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            boolean success = jsonObject.getBoolean("success");
                            if (success) {
                                MaterialDialog dialog1 = new MaterialDialog.Builder(OtpActivity.this)
                                        .title("")
                                        .typeface("Whitney-Semibold-Bas.otf", "Whitney-Semibold-Bas.otf")
                                        .customView(R.layout.dialog_custom_view, true)
                                        .cancelable(false)
                                        .show();
                                TextView txtThanks = (TextView) dialog1.findViewById(R.id.txtThanks);
                                TextView txtRentDone = (TextView) dialog1.findViewById(R.id.txtRentDone);
                                Button btnHome = (Button) dialog1.findViewById(R.id.btnHome);
                                TextView txtReceipt = (TextView) dialog1.findViewById(R.id.txtViewReceipt);
                                Button btnContinue = (Button) dialog1.findViewById(R.id.btnContinue);
                                Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/Whitney-Semibold-Bas.otf");
                                txtThanks.setTypeface(typeface);
                                txtRentDone.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/Whitney-Book-Bas.otf"));
                                btnContinue.setTypeface(typeface);
                                btnHome.setTypeface(typeface);
                                btnHome.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent intent = new Intent(OtpActivity.this, NMCDashboardActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(intent);
                                        finish();
                                    }
                                });
                                btnContinue.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent intent = new Intent(OtpActivity.this, InspectionActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(intent);
                                        finish();
                                    }
                                });
                                txtReceipt.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent intent = new Intent(OtpActivity.this, ReceiptActivity.class);
                                        startActivity(intent);
                                        finish();
                                    }
                                });
                            } else {
                                Snackbar.show(OtpActivity.this, getString(R.string.error_message));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dialog.dismiss();
                        Snackbar.show(OtpActivity.this, getString(R.string.network_error));
                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("id", preferences.getString("license_number", null));
                        params.put("amount", "10");
                        params.put("otp", etOtp.getText().toString());
                        Log.i("id:::::", preferences.getString("license_number", null));
                        return params;
                    }
                };
                request.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                AppController.getInstance().addToRequestQueue(request);
            }
        });
    }
}
