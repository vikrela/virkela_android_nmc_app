package com.tcs.vikrela.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.tcs.vikrela.R;
import com.tcs.vikrela.constants.NetworkConstants;
import com.tcs.vikrela.global.AppController;
import com.tcs.vikrela.global.GPSTracker;
import com.tcs.vikrela.ui.Snackbar;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import java.util.Hashtable;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;

// This activity opens when the NMC officer scans an illegal QR Code and taps on the Register Hawker button

public class CreateNewNMCOfficer extends AppCompatActivity {
    private static final String TAG = "CreateNewNMCOfficer";

    @Bind(R.id.input_FirstName)
    EditText _FirstNameText;
    @Bind(R.id.input_LastName)
    EditText _LastNameText;
    @Bind(R.id.input_email)
    EditText _emailText;
    @Bind(R.id.input_mobile)
    EditText _mobileText;
    @Bind(R.id.android_material_design_spinner)
    MaterialBetterSpinner materialDesignSpinner;
    @Bind(R.id.android_material_design_spinner_division)
    MaterialBetterSpinner materialDesignSpinner1;
    @Bind(R.id.btn_signup)
    Button _signupButton;
    @Bind(R.id.link_login)
    TextView _loginLink;
    /*@Bind(R.id.input_zone)
    EditText _zone;*/
    @Bind(R.id.input_ward)
    EditText _ward;
    @Bind(R.id.input_designation)
    EditText _designation;
    Double latitute;
    Double longitude;

    String[] SPINNERLIST = {"Nasik"};
    GPSTracker gps;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_new_nmcofficer);
        ButterKnife.bind(this);


        _signupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signup();
            }
        });

        _loginLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Finish the registration screen and return to the Login activity
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
            }
        });

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, SPINNERLIST);
        materialDesignSpinner.setAdapter(arrayAdapter);

        String[] division = {getString(R.string.cidco), getString(R.string.satpur),
                getString(R.string.panchvati), getString(R.string.nashik_east), getString(R.string.nashik_west),
                getString(R.string.nasik_road)};
        ArrayAdapter<String> arrayAdapter1 = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, division);
        materialDesignSpinner1.setAdapter(arrayAdapter1);

        gps = new GPSTracker(CreateNewNMCOfficer.this);
        if (gps.canGetLocation()) {
            latitute = gps.getLatitude();
            longitude = gps.getLongitude();
        } else {
            gps.showSettingsAlert();
        }
    }

    public void signup() {
        Log.d(TAG, "Signup");
        if (!validate()) {
            Snackbar.show(CreateNewNMCOfficer.this, getString(R.string.error_message));
            _signupButton.setEnabled(true);
            return;
        } else {
            addAnAccount();
        }
        _signupButton.setEnabled(false);
    }


    public void onSignupSuccess() {
        _signupButton.setEnabled(true);
        setResult(RESULT_OK, null);
        finish();
    }

    // Function used to apply validations to the form for registration of Hawker
    public boolean validate() {
        boolean valid = true;

        String name = _FirstNameText.getText().toString();
        String address = _LastNameText.getText().toString();
        String email = _emailText.getText().toString();
        String mobile = _mobileText.getText().toString();
        //String zone = _zone.getText().toString();
        String ward = _ward.getText().toString();
        String designation = _designation.getText().toString();
        if (name.isEmpty() || name.length() < 3) {
            _FirstNameText.setError(getString(R.string.name_length_error)
            );
            valid = false;
        } else {
            _FirstNameText.setError(null);
        }
        if (address.isEmpty()) {
            _LastNameText.setError(getString(R.string.address_error));
            valid = false;
        } else {
            _LastNameText.setError(null);
        }
        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            _emailText.setError(getString(R.string.email_error));
            valid = false;
        } else {
            _emailText.setError(null);
        }
        if (mobile.isEmpty() || mobile.length() != 10) {
            _mobileText.setError(getString(R.string.mobile_error));
            valid = false;
        } else {
            _mobileText.setError(null);
        }
        if (materialDesignSpinner.getText().toString().equals(getString(R.string.city_name))) {
            materialDesignSpinner.setError(getString(R.string.city_error));
            valid = false;
        } else {
            materialDesignSpinner.setError(null);
        }
        if (materialDesignSpinner1.getText().toString().equals(getString(R.string.division_name))) {
            materialDesignSpinner1.setError(getString(R.string.choose_division));
            valid = false;
        } else {
            materialDesignSpinner1.setError(null);
        }
        /*if (zone.equals("")) {
            _zone.setError(getString(R.string.zone_error));
            valid = false;
        } else {
            _zone.setError(null);
        }*/
        if (ward.equals("")) {
            _ward.setError(getString(R.string.ward_error));
            valid = false;
        } else {
            _ward.setError(null);
        }
        if (designation.equals("")) {
            _designation.setError(getString(R.string.designation_error));
            valid = false;
        } else {
            _designation.setError(null);
        }
        return valid;
    }


    public void addAnAccount() {
        //Showing the progress dialog
        final MaterialDialog dialog = new MaterialDialog.Builder(CreateNewNMCOfficer.this)
                .title(R.string.dialog_register_title)
                .content(R.string.dialog_message)
                .progress(true, 0)
                .show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, NetworkConstants.CREATE_NMC_OFFICER_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                //Showing toast message of the response
                dialog.dismiss();
                Log.d("Response++", s);
                Intent i = new Intent(getApplicationContext(), NMCDashboardActivity.class);
                i.putExtra("alumni_id", s);
                startActivity(i);
                finish();
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        //Dismissing the progress dialog
                        dialog.dismiss();
                        Log.d("VolleyError", "Error is " + volleyError.getMessage());
                        Snackbar.show(CreateNewNMCOfficer.this, getString(R.string.network_error));
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Converting Bitmap to String
                //Creating parameters
                Map<String, String> params = new Hashtable<String, String>();
                params.put("name", _FirstNameText.getText().toString() + " " + _LastNameText.getText().toString());
                params.put("email_id", _emailText.getText().toString());
                params.put("mobile_number", _mobileText.getText().toString());
                params.put("city", materialDesignSpinner.getText().toString());
                //params.put("zone", _zone.getText().toString());
                params.put("ward", _ward.getText().toString());
                params.put("designation", _designation.getText().toString());
                params.put("division", materialDesignSpinner1.getText().toString());
                //returning parameters
                return params;
            }
        };
        //Creating a Request Queue
        AppController.getInstance().addToRequestQueue(stringRequest);
    }
}