package com.tcs.vikrela.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.tcs.vikrela.R;
import com.tcs.vikrela.adapters.HistoryPagerAdapter;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Harsh on 6/8/2017.
 */
public class HistoryActivity extends AppCompatActivity {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.drawer_layout_history)
    DrawerLayout drawerLayoutHistory;
    @Bind(R.id.tabLayoutHistory)
    TabLayout tabLayoutHistory;
    @Bind(R.id.viewpagerHistory)
    ViewPager viewPagerHistory;
    HistoryPagerAdapter historyPagerAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        populate();
    }

    private void populate() {
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.my_history));
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        historyPagerAdapter = new HistoryPagerAdapter(getSupportFragmentManager());
        viewPagerHistory.setAdapter(historyPagerAdapter);
        tabLayoutHistory.setupWithViewPager(viewPagerHistory);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
