package com.tcs.vikrela.activities;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SearchViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;

import com.android.datetimepicker.date.DatePickerDialog;
import com.tcs.vikrela.R;
import com.tcs.vikrela.adapters.RentRecordsAdapter;
import com.tcs.vikrela.dto.PendingDTO;
import com.tcs.vikrela.ui.Snackbar;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Harsh on 6/19/2017.
 */
public class RentRecordsActivity extends AppCompatActivity implements com.android.datetimepicker.date.DatePickerDialog.OnDateSetListener {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.listRentRecords)
    ListView listRentRecords;
    @Bind(R.id.txtChangeDate)
    TextView txtChangeDate;
    @Bind(R.id.txtDate)
    TextView txtDate;
    private List<PendingDTO> data;
    private RentRecordsAdapter adapter;
    private SearchView searchView = null;
    private SearchView.OnQueryTextListener queryTextListener;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rent_records);
        populate();
    }

    private void populate() {
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle("Rent Records");
        final Calendar calendar = Calendar.getInstance();
        String dateString = calendar.get(Calendar.DAY_OF_MONTH) + "/" + (calendar.get(Calendar.MONTH) + 1) + "/" + calendar.get(Calendar.YEAR);
        txtDate.setText(dateString);
        txtChangeDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog.newInstance(RentRecordsActivity.this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)).show(getFragmentManager(), "datePicker");
            }
        });
        txtDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(RentRecordsActivity.this, ReportDownloadActivity.class));
            }
        });
        adapter = new RentRecordsAdapter(RentRecordsActivity.this, getData());
        listRentRecords.setAdapter(adapter);
    }


    public List<PendingDTO> getData() {
        data = new ArrayList<>();
        String[] name = {getString(R.string.shantaram), getString(R.string.shubhwndu), getString(R.string.pratik), getString(R.string.harsh)};
        String[] address = {getString(R.string.cidco), getString(R.string.panchvati), getString(R.string.nasik_road), getString(R.string.nashik_east)};
        String[] amount = {"Rs. 10", "Rs. 10", "Rs. 10", "Rs. 10"};
        String[] id = {"123", "234", "456", "678"};
        for (int i = 0; i < name.length; i++) {
            PendingDTO dto = new PendingDTO();
            dto.setName(name[i]);
            dto.setAddress(address[i]);
            dto.setAmount(amount[i]);
            dto.setId(id[i]);
            data.add(dto);
        }
        return data;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_search_option, menu);
        // Associate searchable configuration with the SearchView
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchManager manager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) searchItem.getActionView();
        searchView.setSearchableInfo(manager.getSearchableInfo(getComponentName()));

        SearchViewCompat.setInputType(searchView, InputType.TYPE_CLASS_NUMBER);
        queryTextListener = new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextChange(String newText) {
                Log.i("onQueryTextChange", newText);
                adapter.filter(newText);
                return false;
            }

            @Override
            public boolean onQueryTextSubmit(String query) {
                Log.i("onQueryTextSubmit", query);
                return false;
            }
        };
        searchView.setOnQueryTextListener(queryTextListener);
        /*SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));*/
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.action_search:
                queryTextListener = new SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextChange(String newText) {
                        Log.i("onQueryTextChange", newText);
                        adapter.filter(newText);
                        return false;
                    }

                    @Override
                    public boolean onQueryTextSubmit(String query) {
                        Log.i("onQueryTextSubmit", query);
                        return false;
                    }
                };
                searchView.setOnQueryTextListener(queryTextListener);
                break;
            default:
                break;
        }
        searchView.setOnQueryTextListener(queryTextListener);
        return true;
    }

    @Override
    public void onDateSet(DatePickerDialog dialog, int year, int monthOfYear, int dayOfMonth) {
        String dateString = dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
        GregorianCalendar gregorianCalendar = new GregorianCalendar(year, monthOfYear, dayOfMonth);
        Date now = new Date();
        now = getDateWithOutTime(now);
        Date date = gregorianCalendar.getTime();
        if (date.compareTo(now) == 1) {
            Snackbar.show(this, getResources().getString(R.string.invalid_date));
            return;
        }
        txtDate.setText(dateString);
    }

    private Date getDateWithOutTime(Date targetDate) {
        Calendar newDate = Calendar.getInstance();
        newDate.setLenient(false);
        newDate.setTime(targetDate);
        newDate.set(Calendar.HOUR_OF_DAY, 0);
        newDate.set(Calendar.MINUTE, 0);
        newDate.set(Calendar.SECOND, 0);
        newDate.set(Calendar.MILLISECOND, 0);
        return newDate.getTime();
    }

}
