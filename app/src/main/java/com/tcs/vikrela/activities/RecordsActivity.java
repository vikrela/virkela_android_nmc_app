package com.tcs.vikrela.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;
import com.tcs.vikrela.R;
import com.tcs.vikrela.constants.NetworkConstants;
import com.tcs.vikrela.global.AppController;
import com.tcs.vikrela.ui.EventDecorator;
import com.tcs.vikrela.ui.Snackbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Harsh on 4/9/2017.
 */
public class RecordsActivity extends AppCompatActivity {
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.calendarView)
    MaterialCalendarView calendarView;
    @Bind(R.id.btnReciept)
    Button btnReciept;
    @Bind(R.id.btnReason)
    Button btnReason;
    @Bind(R.id.btnCollect)
    Button btnCollect;
    @Bind(R.id.btnInspect)
    Button btnInspect;
    @Bind(R.id.llBtn)
    LinearLayout llBtn;
    MaterialDialog dialog;
    Button btnSubmitFeedback;
    CheckBox checkBoxAbsent, checkBoxReason2, checkBoxOthers, checkBoxCollectPayment;
    SharedPreferences preferences;
    final ArrayList<CalendarDay> datesRent = new ArrayList<>();
    final ArrayList<CalendarDay> datesInspection = new ArrayList<>();

    @OnClick(R.id.btnReciept)
    void receipt() {
        startActivity(new Intent(this, ReceiptActivity.class));
    }

    @OnClick(R.id.btnReason)
    void feedback() {
        dialog = new MaterialDialog.Builder(RecordsActivity.this)
                .title(R.string.reason_select)
                .typeface("Whitney-Book-Bas.otf", "Whitney-Book-Bas.otf")
                .customView(R.layout.no_collection_reason_dialog, true).show();
        btnSubmitFeedback = (Button) dialog.findViewById(R.id.btnSubmitFeedback);
        checkBoxAbsent = (CheckBox) dialog.findViewById(R.id.checkboxAbsent);
        checkBoxReason2 = (CheckBox) dialog.findViewById(R.id.checkboxReason2);
        checkBoxOthers = (CheckBox) dialog.findViewById(R.id.checkboxOthers);
        checkBoxCollectPayment = (CheckBox) dialog.findViewById(R.id.checkboxCollectPayment);
        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/Whitney-Book-Bas.otf");
        btnSubmitFeedback.setTypeface(typeface);
        checkBoxAbsent.setTypeface(typeface);
        checkBoxReason2.setTypeface(typeface);
        checkBoxOthers.setTypeface(typeface);
        checkBoxCollectPayment.setTypeface(typeface);

        btnSubmitFeedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((checkBoxAbsent.isChecked() || checkBoxReason2.isChecked() || checkBoxOthers.isChecked()) && (checkBoxCollectPayment.isChecked()))) {
                    Snackbar.show(RecordsActivity.this, getString(R.string.choice_error));
                }
                if (checkBoxAbsent.isChecked() || checkBoxReason2.isChecked() || checkBoxOthers.isChecked()) {
                    dialog.dismiss();
                } else {
                    dialog.dismiss();
                    startActivity(new Intent(RecordsActivity.this, PaymentModeActivity.class));
                }
            }
        });
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_records);
        ButterKnife.bind(this);
        preferences = getSharedPreferences("Vikrela", Context.MODE_PRIVATE);
        populate();
    }

    private void populate() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.view_previous_records);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        btnReciept.setVisibility(View.GONE);
        btnReason.setVisibility(View.GONE);
        btnCollect.setVisibility(View.GONE);
        btnInspect.setVisibility(View.GONE);
        llBtn.setVisibility(View.GONE);
        CalendarDay today = CalendarDay.today();
        calendarView.setSelectedDate(today);
        calendarView.state().edit().setMaximumDate(CalendarDay.today()).commit();

        /*datesRent.add(0, CalendarDay.from(2017, 5, 1));
        datesRent.add(1, CalendarDay.from(2017, 5, 2));
        datesRent.add(2, CalendarDay.from(2017, 5, 3));*/
        /*for (int i = 0; i < 30; i++) {
            CalendarDay day = CalendarDay.from(2017, 0, 1);
            datesRent.add(day);
            calendar.add(Calendar.DATE, 2);
        }*/
        final MaterialDialog dialog1 = new MaterialDialog.Builder(RecordsActivity.this)
                .title(getString(R.string.dialog_title))
                .content(getString(R.string.dialog_content))
                .progress(true, 0)
                .show();
        StringRequest request = new StringRequest(Request.Method.POST, NetworkConstants.RENT_DATES_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                dialog1.dismiss();
                Log.i("Response:::", response);

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("Rent");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        Calendar calendar = Calendar.getInstance();
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                        Date date = simpleDateFormat.parse(jsonObject1.getString("Date"));
                        datesRent.add(i, CalendarDay.from(date));
                        Log.i("Dates:::", datesRent.toString());
                    }
                    JSONArray jsonArray1 = jsonObject.getJSONArray("Inspection");
                    for (int i = 0; i < jsonArray1.length(); i++) {
                        Calendar calendar = Calendar.getInstance();
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
                        JSONObject jsonObject1 = jsonArray1.getJSONObject(i);
                        Date date = simpleDateFormat.parse(jsonObject1.getString("Date"));
                        datesInspection.add(i, CalendarDay.from(date));
                        Log.i("Dates:::", datesInspection.toString());
                    }

                    calendarView.addDecorator(new EventDecorator(getResources().getColor(R.color.colorPrimary), datesRent));
                    calendarView.addDecorator(new EventDecorator(getResources().getColor(R.color.orange), datesInspection));
                    calendarView.setOnDateChangedListener(new OnDateSelectedListener() {
                        @Override
                        public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {
                            if (date.equals(CalendarDay.today())) {
                                llBtn.setVisibility(View.VISIBLE);
                                btnCollect.setVisibility(View.VISIBLE);
                                btnInspect.setVisibility(View.VISIBLE);
                                btnReason.setVisibility(View.GONE);
                                btnReciept.setVisibility(View.GONE);
                            } else if (datesRent.contains(date) || datesInspection.contains(date)) {
                                llBtn.setVisibility(View.GONE);
                                btnInspect.setVisibility(View.GONE);
                                btnCollect.setVisibility(View.GONE);
                                btnReason.setVisibility(View.GONE);
                                btnReciept.setVisibility(View.VISIBLE);
                            } else {
                                llBtn.setVisibility(View.GONE);
                                btnInspect.setVisibility(View.GONE);
                                btnCollect.setVisibility(View.GONE);
                                btnReciept.setVisibility(View.GONE);
                                btnReason.setVisibility(View.VISIBLE);
                            }
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog1.dismiss();
                Snackbar.show(RecordsActivity.this, getString(R.string.network_error));
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                String id = preferences.getString("qr_code_id", null);
                Log.i("id::::", id);
                params.put("id", id);
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(request);

        btnInspect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RecordsActivity.this, InspectionActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("scanned", "yes");
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });

        btnCollect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String license = preferences.getString("hawker_id", null);
                preferences.edit().putString("license_number", license).commit();
                Log.i("id:::", license);
                startActivity(new Intent(RecordsActivity.this, PaymentModeActivity.class));
            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
