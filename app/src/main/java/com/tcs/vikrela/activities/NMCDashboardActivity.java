package com.tcs.vikrela.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.datetimepicker.date.DatePickerDialog;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.tcs.vikrela.R;
import com.tcs.vikrela.fragments.HomeFragment;
import com.tcs.vikrela.ui.Snackbar;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Harsh on 4/6/2017.
 */

// Dashboard for the NMC Officer
public class NMCDashboardActivity extends AppCompatActivity implements com.android.datetimepicker.date.DatePickerDialog.OnDateSetListener {

    @Bind(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @Bind(R.id.navigation_view)
    NavigationView navigationView;
    private MenuItem previousMenuItem;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    private SharedPreferences preferences;
    int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard_nmc);
        populate();
    }

    private void populate() {
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        preferences = getSharedPreferences("Vikrela", Context.MODE_PRIVATE);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // setting up the navigation drawer functionality
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                if (previousMenuItem != null) {
                    previousMenuItem.setChecked(false);
                }
                item.setCheckable(true);
                item.setChecked(true);
                previousMenuItem = item;
                drawerLayout.closeDrawers();

                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                switch (item.getItemId()) {
                    case R.id.home:
                        HomeFragment homeFragment = new HomeFragment();
                        fragmentTransaction.replace(R.id.frame, homeFragment);
                        fragmentTransaction.commit();
                        return true;
                    case R.id.rent_records:
                        startActivity(new Intent(NMCDashboardActivity.this, RentRecordsActivity.class));
                        return true;
                    case R.id.inspection_records:
                        startActivity(new Intent(NMCDashboardActivity.this, InspectionRecordsActivity.class));
                        return true;
                    case R.id.inspectHawker:
                        startActivity(new Intent(NMCDashboardActivity.this, InspectionActivity.class));
                        return true;
                    case R.id.inspectFacilities:
                        startActivity(new Intent(NMCDashboardActivity.this, InspectFacilitiesActivity.class));
                        return true;
                    case R.id.myHistory:
                        startActivity(new Intent(NMCDashboardActivity.this, HistoryActivity.class));
                        return true;
                    case R.id.about:
                        startActivity(new Intent(NMCDashboardActivity.this, AboutUsActivity.class));
                        return true;
                    case R.id.terms:
                        startActivity(new Intent(NMCDashboardActivity.this, TermsAndConditionsActivity.class));
                        return true;
                    case R.id.logout:
                        new MaterialDialog.Builder(NMCDashboardActivity.this)
                                .title(R.string.dialog_logout_title)
                                .content(R.string.dialog_logout_content)
                                .positiveText(R.string.yes)
                                .negativeText(R.string.cancel)
                                .onPositive(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                        startActivity(new Intent(NMCDashboardActivity.this, LoginActivity.class));
                                        preferences.edit().clear().commit();
                                        ActivityCompat.finishAffinity(NMCDashboardActivity.this);
                                    }
                                })
                                .onNegative(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                        dialog.dismiss();
                                    }
                                })
                                .typeface("Whitney-Semibold-Bas.otf", "Whitney-Semibold-Bas.otf")
                                .show();
                        return true;
                    default:
                        return true;
                }
            }
        });

        // displaying home by default
        HomeFragment fragment = new HomeFragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame, fragment);
        transaction.commit();

        // Getting those three parallel menu lines to move on drawer action
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.openDrawer, R.string.closeDrawer) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };

        drawerLayout.setDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_nmc_dashboard, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                break;

            // Using google to autocomplete the address
            case R.id.area:
                try {
                    Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN).build(this);
                    startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
                } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }
                break;

            case R.id.date:
                Calendar calendar = Calendar.getInstance();
                DatePickerDialog.newInstance(this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)).show(getFragmentManager(), "datePicker");
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Fragment f = getSupportFragmentManager().findFragmentById(R.id.frame);
        if (f instanceof HomeFragment)
            super.onBackPressed();
        else {
            Intent intent = new Intent(NMCDashboardActivity.this, NMCDashboardActivity.class);
            startActivity(intent);
            finish();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                /*Intent intent = new Intent(this, HistoryActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("place", place.getName().toString());
                intent.putExtras(bundle);
                startActivity(intent);*/
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onDateSet(DatePickerDialog dialog, int year, int monthOfYear, int dayOfMonth) {
        String dateString = dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
        GregorianCalendar gregorianCalendar = new GregorianCalendar(year, monthOfYear, dayOfMonth);
        Date now = new Date();
        now = getDateWithOutTime(now);
        Date date = gregorianCalendar.getTime();
        if (date.compareTo(now) == 1) {
            Snackbar.show(this, getResources().getString(R.string.invalid_date));
            return;
        }
        Intent intent = new Intent(this, HistoryActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("date", date.toString());
        startActivity(intent);
    }

    private Date getDateWithOutTime(Date targetDate) {
        Calendar newDate = Calendar.getInstance();
        newDate.setLenient(false);
        newDate.setTime(targetDate);
        newDate.set(Calendar.HOUR_OF_DAY, 0);
        newDate.set(Calendar.MINUTE, 0);
        newDate.set(Calendar.SECOND, 0);
        newDate.set(Calendar.MILLISECOND, 0);

        return newDate.getTime();

    }
}
