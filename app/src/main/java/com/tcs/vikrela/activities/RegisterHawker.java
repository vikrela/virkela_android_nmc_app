package com.tcs.vikrela.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.tcs.vikrela.R;
import com.tcs.vikrela.constants.NetworkConstants;
import com.tcs.vikrela.global.AppController;
import com.tcs.vikrela.global.GPSTracker;
import com.tcs.vikrela.ui.Snackbar;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RegisterHawker extends AppCompatActivity {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.imgHawkerProfile)
    ImageView imgHawkerProfile;
    @Bind(R.id.etHawkerName)
    TextInputEditText etHawkerName;
    @Bind(R.id.radioGroupGender)
    RadioGroup radioGroupGender;
    @Bind(R.id.radiobtnMale)
    RadioButton radioButtonMale;
    @Bind(R.id.radiobtnFemale)
    RadioButton radioButtonFemale;
    @Bind(R.id.checkboxMarital)
    CheckBox checkBoxMarital;
    @Bind(R.id.etHawkerAddress)
    TextInputEditText etHawkerAddress;
    @Bind(R.id.etHawkerMobile)
    TextInputEditText etHawkerMobile;
    @Bind(R.id.spinnerAddressProof)
    Spinner spinnerAddressProof;
    @Bind(R.id.imgCaptureAddressProof)
    ImageView imgCaptureAddressProof;
    @Bind(R.id.spinnerIdProof)
    Spinner spinnerIdProof;
    @Bind(R.id.imgCaptureIdProof)
    ImageView imgCaptureIdProof;
    @Bind(R.id.radioGroupHandicap)
    RadioGroup radioGroupHandicap;
    @Bind(R.id.radiobtnHandicapYes)
    RadioButton radioButtonHandicapYes;
    @Bind(R.id.radiobtnHandicapNo)
    RadioButton radioButtonHandicapNo;
    @Bind(R.id.spinnerCategory)
    Spinner spinnerCategory;
    @Bind(R.id.btnProceedPersonal)
    Button btnProceedPersonal;
    Bitmap bitmap, bitmap1, bitmap2;
    String bmpProfile, bmpAddressProof, bmpIdProof;
    String gender, maritalStatus = "", handicap, id;
    GPSTracker tracker;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_hawker);
        populate();
    }

    private void populate() {
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.register_hawker);
        tracker = new GPSTracker(RegisterHawker.this);
        imgHawkerProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, 1000);
            }
        });

        imgCaptureAddressProof.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, 1500);
            }
        });

        imgCaptureIdProof.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, 1700);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1000 && resultCode == RESULT_OK) {
            bitmap = (Bitmap) data.getExtras().get("data");
            imgHawkerProfile.setImageBitmap(bitmap);
        }
        if (requestCode == 1500 && resultCode == RESULT_OK) {
            bitmap1 = (Bitmap) data.getExtras().get("data");
            imgCaptureAddressProof.setImageBitmap(bitmap1);
        }
        if (requestCode == 1700 && resultCode == RESULT_OK) {
            bitmap2 = (Bitmap) data.getExtras().get("data");
            imgCaptureIdProof.setImageBitmap(bitmap2);
        }
    }

    public String getStringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 40, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.btnProceedPersonal)
    void proceedPersonal() {
        Bundle bundle = getIntent().getExtras();
        if (null != bundle) {
            if (bundle.containsKey("id")) {
                id = bundle.getString("id");
            }
        }
        if (etHawkerName.getText().toString().equals("")) {
            Snackbar.show(this, getString(R.string.name_error));
            etHawkerName.requestFocus();
            etHawkerName.setError(getString(R.string.name));
            return;
        }
        if (etHawkerAddress.getText().toString().equals("")) {
            Snackbar.show(this, getString(R.string.address_error));
            etHawkerAddress.requestFocus();
            etHawkerAddress.setError(getString(R.string.address));
            return;
        }
        if (etHawkerMobile.getText().toString().equals("")) {
            Snackbar.show(this, getString(R.string.mobile_error));
            etHawkerMobile.requestFocus();
            etHawkerMobile.setError(getString(R.string.mobile));
            return;
        }
        if (etHawkerMobile.getText().toString().length() != 10) {
            Snackbar.show(this, getString(R.string.mobile_length_error));
            etHawkerMobile.requestFocus();
            etHawkerMobile.setError(getString(R.string.mobile_length));
            return;
        } else {
            if (radioButtonMale.isChecked()) {
                gender = getString(R.string.male);
                if (checkBoxMarital.isChecked()) {
                    maritalStatus = getString(R.string.widower);
                }
            } else {
                gender = getString(R.string.female);
                if (checkBoxMarital.isChecked()) {
                    maritalStatus = getString(R.string.widow);
                }
            }
            if (radioButtonHandicapYes.isChecked()) {
                handicap = getString(R.string.yes);
            } else {
                handicap = getString(R.string.no);
            }
            if (bitmap != null) {
                bmpProfile = getStringImage(bitmap);
            }
            if (bitmap1 != null) {
                bmpAddressProof = getStringImage(bitmap1);
            }
            if (bitmap2 != null) {
                bmpIdProof = getStringImage(bitmap2);
            }
            register(id, etHawkerName.getText().toString(), gender + " " + maritalStatus, etHawkerAddress.getText().toString(),
                    etHawkerMobile.getText().toString(), spinnerAddressProof.getSelectedItem().toString(),
                    bmpAddressProof, spinnerIdProof.getSelectedItem().toString(), bmpIdProof, bmpProfile, handicap,
                    spinnerCategory.getSelectedItem().toString());
        }
    }

    private void register(final String id, final String hawkerName, final String hawkerGender, final String hawkerAddress, final String hawkerMobile,
                          final String addressProof, final String addressProofImage, final String idProof, final String idProofImage,
                          final String profileImage, final String handicap, final String hawkerCategory) {


        final MaterialDialog dialog = new MaterialDialog.Builder(RegisterHawker.this)
                .title(getString(R.string.dialog_register_title))
                .content(getString(R.string.dialog_message))
                .progress(true, 0)
                .cancelable(false)
                .show();


        StringRequest stringRequest = new StringRequest(Request.Method.POST, NetworkConstants.REGISTER_HAWKER, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("Response:::", response);
                dialog.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int success = jsonObject.getInt("success");
                    if (success == 1) {
                        Intent intent = new Intent(RegisterHawker.this, HawkerBusinessDetailsActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("mobile", hawkerMobile);
                        bundle.putString("id", id);
                        intent.putExtras(bundle);
                        startActivity(intent);
                        finish();
                    } else if (success == 0) {
                        Snackbar.show(RegisterHawker.this, getString(R.string.error_message));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Error:::", error.toString());
                dialog.dismiss();
                Snackbar.show(RegisterHawker.this, getString(R.string.network_error));
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                double lat = 0, lon = 0;
                if (tracker.canGetLocation()) {
                    lat = tracker.getLatitude();
                    lon = tracker.getLongitude();
                }
                Map<String, String> params = new HashMap<String, String>();
                params.put("id", id);
                params.put("HawkerProfilePicture", profileImage);
                params.put("Name", hawkerName);
                params.put("Gender", hawkerGender);
                params.put("Address", hawkerAddress);
                params.put("Mobile", hawkerMobile);
                params.put("ResidentialIdProofType", addressProof);
                params.put("ResidentialProof", addressProofImage);
                params.put("IdProofType", idProof);
                params.put("IdProof", idProofImage);
                params.put("Handicap", handicap);
                params.put("Caste", hawkerCategory);
                params.put("latitude", String.valueOf(lat));
                params.put("longitude", String.valueOf(lon));
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(stringRequest);
    }
}
