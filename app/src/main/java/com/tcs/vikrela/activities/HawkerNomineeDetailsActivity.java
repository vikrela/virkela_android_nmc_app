package com.tcs.vikrela.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.tcs.vikrela.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Harsh on 6/8/2017.
 */

// Simple form for filling the nominee of the hawker enrolled.

public class HawkerNomineeDetailsActivity extends AppCompatActivity {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.btnFillBankDetails)
    Button btnFillBankDetails;
    @Bind(R.id.etNomineeName)
    EditText etNomineeName;
    @Bind(R.id.etNomineePhone)
    EditText etNomineePhone;
    @Bind(R.id.etNomineeAddress)
    EditText etNomineeAddress;
    @Bind(R.id.radioNomineeGender)
    RadioGroup radioNomineeGender;
    @Bind(R.id.radioFemale)
    RadioButton rdbtnFemale;
    @Bind(R.id.radioMale)
    RadioButton rdbtnMale;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hawker_nominee_details);
        populate();
    }

    private void populate() {
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.fill_nominee_name);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // button to enter the form for filling bank details
        btnFillBankDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(HawkerNomineeDetailsActivity.this, HawkerBankDetailsActivity.class));
                finish();
            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
