package com.tcs.vikrela.activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.tcs.vikrela.R;
import com.tcs.vikrela.constants.NetworkConstants;
import com.tcs.vikrela.global.AppController;
import com.tcs.vikrela.ui.Snackbar;
import com.tcs.vikrela.util.NetworkCheck;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Harsh on 4/5/2017.
 */

public class LoginActivity extends AppCompatActivity {

    @Bind(R.id.btnProceed)
    Button btnProceed;
    @Bind(R.id.txtLoginPrefInfo)
    TextView txtLoginPrefInfo;
    @Bind(R.id.etNMCId)
    EditText etNMCId;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.txtRegister)
    TextView txtRegister;
    @Bind(R.id.llLogin)
    LinearLayout llLogin;
    SharedPreferences preferences;

    @OnClick(R.id.btnProceed)
    void enter() {
        if (!NetworkCheck.isNetworkAvailable(this)) {
            Snackbar.show(this, getString(R.string.no_internet));
            return;
        }
        if (etNMCId.getText().toString().equals("")) {
            Snackbar.show(this, getString(R.string.empty_field_error));
            return;
        }
        login(etNMCId.getText().toString());

    }

    private void login(final String nmcID) {
        final String TAG = "string_req";
        final MaterialDialog dialog = new MaterialDialog.Builder(LoginActivity.this)
                .title(R.string.dialog_login_title)
                .content(getString(R.string.dialog_content))
                .progress(true, 0)
                .cancelable(false)
                .show();

        // Volley Request for login
        StringRequest request = new StringRequest(Request.Method.POST, NetworkConstants.NMC_LOGIN, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response);
                dialog.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    boolean res = jsonObject.getBoolean("success");
                    if (res) {
                        llLogin.setVisibility(View.INVISIBLE);
                        String division = jsonObject.getString("divison");
                        String id = jsonObject.getString("id");
                        preferences.edit().putString("division", division).commit();
                        preferences.edit().putString("nmc_id", id).commit();
                        startActivity(new Intent(LoginActivity.this, NMCDashboardActivity.class));
                        finish();
                    } else {
                        Snackbar.show(LoginActivity.this, getString(R.string.invalid_credentials));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                Log.e(TAG, error.toString());
                Snackbar.show(LoginActivity.this, getString(R.string.network_error));
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("lisence_no", nmcID);
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(request);
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        preferences = getSharedPreferences("Vikrela", Context.MODE_PRIVATE);
        //Requesting permissions for the application for android >= Lollipop
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_CONTACTS,
                Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.SEND_SMS,
                Manifest.permission.READ_SMS,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.RECEIVE_SMS}, 1);
        populate();
    }

    private void populate() {
        ButterKnife.bind(this);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/Whitney-Book-Bas.otf");
        etNMCId.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        etNMCId.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/Whitney-Semibold-Bas.otf"));
        txtLoginPrefInfo.setTypeface(typeface);
        txtRegister.setTypeface(typeface);
        btnProceed.setTypeface(typeface);
        txtRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, CreateNewNMCOfficer.class));
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 1:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                        grantResults[1] == PackageManager.PERMISSION_GRANTED &&
                        grantResults[2] == PackageManager.PERMISSION_GRANTED &&
                        grantResults[3] == PackageManager.PERMISSION_GRANTED &&
                        grantResults[4] == PackageManager.PERMISSION_GRANTED &&
                        grantResults[5] == PackageManager.PERMISSION_GRANTED &&
                        grantResults[6] == PackageManager.PERMISSION_GRANTED &&
                        grantResults[7] == PackageManager.PERMISSION_GRANTED &&
                        grantResults[8] == PackageManager.PERMISSION_GRANTED) {

                } else {
                    Toast.makeText(LoginActivity.this, R.string.permissions_required, Toast.LENGTH_SHORT).show();
                    ActivityCompat.finishAffinity(this);
                }
                break;
        }
    }
}
