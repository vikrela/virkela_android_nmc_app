package com.tcs.vikrela.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.tcs.vikrela.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Harsh on 6/16/2017.
 */

// About us menu in navigation drawer is displayed using this activity

public class AboutUsActivity extends AppCompatActivity{

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.webViewAbout)
    WebView webViewAbout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_PROGRESS);
        setContentView(R.layout.activity_about_us);
        populate();
    }

    private void populate() {
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getWindow().setFeatureInt(Window.FEATURE_PROGRESS, Window.PROGRESS_VISIBILITY_ON);
        getSupportActionBar().setTitle(getString(R.string.about_us));
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        webViewAbout.getSettings().setJavaScriptEnabled(true);
        webViewAbout.getSettings().setSupportZoom(true);
        webViewAbout.setWebViewClient(new Callback());
        webViewAbout.loadUrl("https://www.google.com");
    }

    // forcing the webview to open inside your app instead of phone's default browser
    private class Callback extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            return (false);
        }
    }

    // Handling back press using the menu options
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}
