package com.tcs.vikrela.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.tcs.vikrela.R;
import com.tcs.vikrela.constants.NetworkConstants;
import com.tcs.vikrela.global.AppController;
import com.tcs.vikrela.global.GPSTracker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Harsh on 4/9/2017.
 */

// The main dashboard after scanning or entering license number. Every action redirects from here

public class HawkerInfoActivity extends AppCompatActivity {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.btnViewRecords)
    Button btnViewRecords;
    @Bind(R.id.infoView)
    RelativeLayout infoView;
    @Bind(R.id.cardHawkerInfo)
    CardView cardHawkerInfo;
    @Bind(R.id.btnRegisterHawker)
    Button btnRegisterHawker;
    @Bind(R.id.txtNoData)
    TextView txtNoData;
    String id = new String();
    TextView txtNameVal, txtMobileVal, txtTypeVal, txtPeriod, txtStreetVal, txtAddressVal, txtDOBVal, txtGenderVal, txtSelectCityVal, txtHandicapped, txtCasteVal;
    MaterialDialog dialog;
    SharedPreferences preferences;
    String TAG = "HawkerInfo";
    GPSTracker tracker;
    double lat, lon;
    String hasQr = "";

    // this takes you to the calendar from where you can view the previous records of a particular hawker.
    @OnClick(R.id.btnViewRecords)
    void records() {
        Intent intent = new Intent(this, RecordsActivity.class);
        startActivity(intent);
    }

/*

    @OnClick(R.id.btnShareHawkerInfo)
    void share() {
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        String shareBody = "Here is the share content body";
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject Here");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
        startActivity(Intent.createChooser(sharingIntent, "Share via"));
    }
*/

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hawker_info);
        ButterKnife.bind(this);
        tracker = new GPSTracker(HawkerInfoActivity.this);
        preferences = getSharedPreferences("Vikrela", Context.MODE_PRIVATE);
        infoView.setVisibility(View.INVISIBLE);
        btnRegisterHawker.setVisibility(View.GONE);
        txtNoData.setVisibility(View.GONE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.hawker_info);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        dialog = new MaterialDialog.Builder(HawkerInfoActivity.this)
                .title(R.string.dialog_title)
                .content(R.string.dialog_content)
                .progress(true, 0)
                .show();
        txtNameVal = (TextView) findViewById(R.id.txtNameVal);
        txtMobileVal = (TextView) findViewById(R.id.txtMobileVal);
        txtTypeVal = (TextView) findViewById(R.id.txtTypeVal);
        txtDOBVal = (TextView) findViewById(R.id.txtDOBVal);
        txtGenderVal = (TextView) findViewById(R.id.txtGenderVal);
        txtSelectCityVal = (TextView) findViewById(R.id.txtSelectCityVal);
        txtHandicapped = (TextView) findViewById(R.id.txtHandicappedVal);
        txtCasteVal = (TextView) findViewById(R.id.txtCasteVal);
        txtPeriod = (TextView) findViewById(R.id.txtDOBVal);
        txtStreetVal = (TextView) findViewById(R.id.txtGenderVal);
        txtAddressVal = (TextView) findViewById(R.id.txtSelectCityVal);
        Bundle bundle = getIntent().getExtras();
        if (null != bundle) {
            if (bundle.containsKey("URL")) {
                String url = bundle.getString("URL");
                StringTokenizer str = new StringTokenizer(url, "id=");
                while (str.hasMoreElements()) {
                    id = str.nextElement().toString();
                    preferences.edit().putString("qr_code_id", id).commit();
                }
                populate(id);
            }
            if (bundle.containsKey("license")) {
                id = bundle.getString("license");
                viaLicense(id);
                preferences.edit().putString("qr_code_id", "").commit();
            }
        }

    }

    private void viaLicense(final String id) {
        StringRequest jsonReq = new StringRequest(Request.Method.POST,
                NetworkConstants.INFORMATION_URL_ID, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Response: " + response);
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(response);
                    parseJsonFeed(jsonObject);
                } catch (JSONException e) {
                    Log.d(TAG, "Catch" + e.getMessage());
                    e.printStackTrace();
                }
            }

        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "Error: " + error.getMessage());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                //if(bundle!=null)
                if (tracker.canGetLocation()) {
                    lat = tracker.getLatitude();
                    lon = tracker.getLongitude();
                } else {

                }
                params.put("id", id);
                params.put("latitude", String.valueOf(lat));
                params.put("longitude", String.valueOf(lon));

                //else
                //    Log.d("Bundle","Bundle is null");
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(jsonReq);
    }

    private void populate(final String id) {

        StringRequest jsonReq = new StringRequest(Request.Method.POST,
                NetworkConstants.INFORMATION_URL, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Response: " + response);
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(response);
                    parseJsonFeed(jsonObject);
                } catch (JSONException e) {
                    Log.d(TAG, "Catch" + e.getMessage());
                    e.printStackTrace();
                }
            }

        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "Error: " + error.getMessage());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                //if(bundle!=null)
                if (tracker.canGetLocation()) {
                    lat = tracker.getLatitude();
                    lon = tracker.getLongitude();
                }
                params.put("id", id);
                params.put("latitude", String.valueOf(lat));
                params.put("longitude", String.valueOf(lon));
                //else
                //    Log.d("Bundle","Bundle is null");
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(jsonReq);
    }

    /**
     * Parsing json reponse and passing the data to feed view list adapter
     */
    private void parseJsonFeed(JSONObject response) {
        try {
            JSONArray feedArray = response.getJSONArray("result");
            infoView.setVisibility(View.VISIBLE);
            dialog.dismiss();
            for (int i = 0; i < feedArray.length(); i++) {
                JSONObject feedObj = (JSONObject) feedArray.get(i);
                preferences.edit().putString("hawker_id", feedObj.getString("id")).commit();
                txtNameVal.setText(feedObj.getString("name"));
                txtMobileVal.setText(feedObj.getString("mobile"));
                txtTypeVal.setText(feedObj.getString("type"));
                txtHandicapped.setText(feedObj.getString("handicap"));
                txtCasteVal.setText(feedObj.getString("caste"));
                txtPeriod.setText(feedObj.getString("period") + " years");
                txtGenderVal.setText(feedObj.getString("street_name"));
                txtAddressVal.setText(feedObj.getString("business_place"));
            }

            if (feedArray.length() == 0) {
                dialog.dismiss();
                preferences.edit().putString("hawker_id", id).commit();
                infoView.setVisibility(View.VISIBLE);
                cardHawkerInfo.setVisibility(View.GONE);
                txtNoData.setVisibility(View.VISIBLE);
                btnRegisterHawker.setVisibility(View.VISIBLE);
                btnRegisterHawker.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(HawkerInfoActivity.this, RegisterHawker.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("id", id);
                        intent.putExtras(bundle);
                        startActivity(intent);
                        finish();
                    }
                });
            }
            // notify data changes to list adapater
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_hawker_info, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.share:
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                String shareBody = "Here is the share content body";
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject Here");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(sharingIntent, "Share via"));
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
