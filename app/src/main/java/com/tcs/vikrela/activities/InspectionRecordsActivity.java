package com.tcs.vikrela.activities;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;

import com.android.datetimepicker.date.DatePickerDialog;
import com.tcs.vikrela.R;
import com.tcs.vikrela.adapters.InspectionReportAdapter;
import com.tcs.vikrela.dto.ReportDTO;
import com.tcs.vikrela.ui.Snackbar;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

// This activity displays the previous records for the logged in NMC officer by date and also allows him to view data for particular date
public class InspectionRecordsActivity extends AppCompatActivity implements com.android.datetimepicker.date.DatePickerDialog.OnDateSetListener {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.txtChangeDate)
    TextView txtChangeDate;
    @Bind(R.id.txtDate)
    TextView txtDate;
    @Bind(R.id.listInspectionRecords)
    ListView listInspectionRecords;
    private InspectionReportAdapter adapter;
    private List<ReportDTO> reportDTOList;
    SearchView searchView = null;
    private SearchView.OnQueryTextListener queryTextListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inspection_records);
        populate();
    }

    private void populate() {
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.inspection_records));
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final Calendar calendar = Calendar.getInstance();
        String dateString = calendar.get(Calendar.DAY_OF_MONTH) + "/" + (calendar.get(Calendar.MONTH) + 1) + "/" + calendar.get(Calendar.YEAR);
        txtDate.setText(dateString);
        txtChangeDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog.newInstance(InspectionRecordsActivity.this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)).show(getFragmentManager(), "datePicker");
            }
        });
        txtDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(InspectionRecordsActivity.this, ReportDownloadActivity.class));
            }
        });
        adapter = new InspectionReportAdapter(InspectionRecordsActivity.this, getData());
        listInspectionRecords.setAdapter(adapter);
    }


    // this sets up the static data for the listview
    public List<ReportDTO> getData() {
        reportDTOList = new ArrayList<>();
        String[] names = {"Shubhendu Awasthi", "Pratik Khanapurkar", "Harsh Deep Singh", "Vivek", "Kritika Mengi"};
        String[] address = {"Kanpur", "Nasik", "Meerut", "Ahemdabad", "Jammu"};
        String[] violation = {"Violation 2", "Enforcement", "No Violation", "Enforcement", "Violation 1"};
        String[] id = {"123", "234", "345", "567", "678"};
        for (int i = 0; i < names.length; i++) {
            ReportDTO dto = new ReportDTO();
            dto.setName(names[i]);
            dto.setAddress(address[i]);
            dto.setViolation(violation[i]);
            dto.setId(id[i]);
            reportDTOList.add(dto);
        }
        return reportDTOList;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_search_option, menu);
        // Associate searchable configuration with the SearchView
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchManager manager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) searchItem.getActionView();
        searchView.setSearchableInfo(manager.getSearchableInfo(getComponentName()));

        queryTextListener = new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextChange(String newText) {
                Log.i("onQueryTextChange", newText);
                adapter.filter(newText);
                return false;
            }

            @Override
            public boolean onQueryTextSubmit(String query) {
                Log.i("onQueryTextSubmit", query);
                return false;
            }
        };
        searchView.setOnQueryTextListener(queryTextListener);
        /*SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));*/
        return true;
    }

    // setting up date in textview
    @Override
    public void onDateSet(DatePickerDialog dialog, int year, int monthOfYear, int dayOfMonth) {
        String dateString = dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
        GregorianCalendar gregorianCalendar = new GregorianCalendar(year, monthOfYear, dayOfMonth);
        Date now = new Date();
        now = getDateWithOutTime(now);
        Date date = gregorianCalendar.getTime();
        // since the records are only present till today then date also should not exceed today
        if (date.compareTo(now) == 1) {
            Snackbar.show(this, getResources().getString(R.string.invalid_date));
            return;
        }
        txtDate.setText(dateString);

    }

    // getting date in a particular format i.e. dd/mm/yyyy and not complete system provided date
    private Date getDateWithOutTime(Date targetDate) {
        Calendar newDate = Calendar.getInstance();
        newDate.setLenient(false);
        newDate.setTime(targetDate);
        newDate.set(Calendar.HOUR_OF_DAY, 0);
        newDate.set(Calendar.MINUTE, 0);
        newDate.set(Calendar.SECOND, 0);
        newDate.set(Calendar.MILLISECOND, 0);
        return newDate.getTime();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
