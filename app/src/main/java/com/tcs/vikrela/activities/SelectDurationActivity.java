package com.tcs.vikrela.activities;

import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.NotificationCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.tcs.vikrela.R;
import com.tcs.vikrela.ui.MySelectorDecorator;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class SelectDurationActivity extends AppCompatActivity {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.calendarViewReport)
    MaterialCalendarView calendarViewReport;
    @Bind(R.id.btnProceedDownload)
    Button btnProceedDownload;
    private ProgressDialog pDialog;
    public static final int progress_bar_type = 0;
    long total = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_duration);
        populate();
    }

    private void populate() {
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle("Select Duration");
        calendarViewReport.setSelectionMode(MaterialCalendarView.SELECTION_MODE_RANGE);
        CalendarDay today = CalendarDay.today();
        calendarViewReport.setSelectedDate(today);
        calendarViewReport.state().edit().setMaximumDate(CalendarDay.today()).commit();
        calendarViewReport.addDecorator(new MySelectorDecorator(SelectDurationActivity.this));

        btnProceedDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String extStorageDirectory = Environment.getExternalStorageDirectory()
                        .toString();
                File folder = new File(extStorageDirectory, "pdf");
                folder.mkdir();
                File file = new File(folder, "maven.pdf");
                try {
                    file.createNewFile();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
                new DownloadFileFromURL().execute("http://maven.apache.org/maven-1.x/maven.pdf");

            }
        });
    }

    class DownloadFileFromURL extends AsyncTask<String, String, String> {
        NotificationManager mNotifyManager;
        NotificationCompat.Builder mBuilder;

        /**
         * Before starting background thread
         * Show Progress Bar Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //showDialog(progress_bar_type);
            mNotifyManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
            mBuilder = new NotificationCompat.Builder(getApplicationContext());
            mBuilder.setContentTitle("File Download")
                    .setContentText("Downloading File..")
                    .setSmallIcon(R.drawable.ic_abc_vikrela_app_logo)
                    .setProgress(100, 0, false);
            mNotifyManager.notify(0, mBuilder.build());
            Toast.makeText(getApplicationContext(), "Downloading the file...", Toast.LENGTH_LONG).show();
        }

        /**
         * Downloading file in background thread
         */
        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {
                URL url = new URL(f_url[0]);
                URLConnection conection = url.openConnection();
                conection.connect();
                // this will be useful so that you can show a tipical 0-100% progress bar
                int lenghtOfFile = conection.getContentLength();

                // download the file
                InputStream input = new BufferedInputStream(url.openStream(), 8192);

                // Output stream
                OutputStream output = new FileOutputStream(Environment.getExternalStorageDirectory() + "/pdf/maven.pdf");

                byte data[] = new byte[1024];

                //long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }

            return null;
        }

        /**
         * Updating progress bar
         */
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
            //pDialog.setProgress(Integer.parseInt(progress[0]));
            mBuilder.setProgress(100, Integer.parseInt(progress[0]), false);
            // Displays the progress bar on notification
            mNotifyManager.notify(0, mBuilder.build());
            super.onProgressUpdate(progress);
        }

        /**
         * After completing background task
         * Dismiss the progress dialog
         **/
        @Override
        protected void onPostExecute(String file_url) {
            mBuilder.setContentText("Download Complete")
                    .setProgress(0, 0, false);
            // Removes the progress bar
            mNotifyManager.notify(0, mBuilder.build());
            // dismiss the dialog after the file was downloaded
            //dismissDialog(progress_bar_type);

            // Displaying downloaded image into image view
            // Reading image path from sdcard
            //String path = Environment.getExternalStorageDirectory().toString() + "/maven.pdf";
            // setting downloaded into image view
            File file = new File(Environment.getExternalStorageDirectory() + "/pdf/maven.pdf");
            PackageManager packageManager = getPackageManager();
            Intent testIntent = new Intent(Intent.ACTION_VIEW);
            testIntent.setType("application/pdf");
            List list = packageManager.queryIntentActivities(testIntent, PackageManager.MATCH_DEFAULT_ONLY);
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_VIEW);
            Uri uri = Uri.fromFile(file);
            intent.setDataAndType(uri, "application/pdf");
            startActivity(intent);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
