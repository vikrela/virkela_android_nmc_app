package com.tcs.vikrela.constants;

/**
 * Created by Harsh on 6/5/2017.
 */
public interface NetworkConstants {
    public String NETWORK_IP = "http://kkwagheducationsocietyalumni.in/Vikrela/Android/";
    public String NMC_LOGIN = NETWORK_IP + "/LoginViaLisenceNo.php";
    public String INFORMATION_URL = NETWORK_IP + "/retriveHawkerData.php";
    public String INFORMATION_URL_ID = NETWORK_IP + "/retriveHawkerDataFromLisenceNo.php";
    public String CREATE_NMC_OFFICER_URL = NETWORK_IP + "/createARentCollectingOfficial.php";
    public String REGISTER_HAWKER = NETWORK_IP + "/createAHawker.php";
    public String BUSINESS_DETAILS_URL = NETWORK_IP + "/CompleteCreateNewHawker.php";
    public String RENT_URL = NETWORK_IP + "/retriveHawkersWhoPaidRent.php";
    public String PAY_RENT_URL = NETWORK_IP + "/RentCollection.php";
    public String RENT_DATES_URL = NETWORK_IP + "/DatesOnWhichRentWasPaid.php";
    public String INSPECTION_URL = NETWORK_IP + "/TempInspection.php";
    public String GET_OTP_URL = NETWORK_IP + "/OTPGeneration.php";
}
