package com.tcs.vikrela.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.tcs.vikrela.R;
import com.tcs.vikrela.adapters.PendingAdapter;
import com.tcs.vikrela.dto.PendingDTO;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class PendingFragment extends Fragment {

    @Bind(R.id.recyclerViewPending)
    RecyclerView recyclerViewPending;
    private PendingAdapter adapter;
    private List<PendingDTO> data;

    public PendingFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View parentView = inflater.inflate(R.layout.fragment_pending, container, false);
        ButterKnife.bind(this, parentView);
        populate();
        return parentView;
    }

    private void populate() {
        adapter = new PendingAdapter(getActivity(), getData(), new PendingAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(PendingDTO pendingDTO) {
                Toast.makeText(getActivity(), pendingDTO.getName() + " Clicked", Toast.LENGTH_SHORT).show();
            }
        });
        recyclerViewPending.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        recyclerViewPending.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    public List<PendingDTO> getData() {
        data = new ArrayList<>();
        String[] name = {getString(R.string.shantaram), getString(R.string.shubhwndu), getString(R.string.pratik), getString(R.string.harsh)};
        String[] address = {getString(R.string.cidco), getString(R.string.panchvati), getString(R.string.nasik_road), getString(R.string.nashik_east)};
        String[] amount = {"Rs. 20", "Rs. 30", "Rs. 60", "Rs. 45"};
        for (int i = 0; i < name.length; i++) {
            PendingDTO dto = new PendingDTO();
            dto.setName(name[i]);
            dto.setAddress(address[i]);
            dto.setAmount(amount[i]);
            data.add(dto);
        }
        return data;
    }
}
