package com.tcs.vikrela.fragments;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.tcs.vikrela.R;
import com.tcs.vikrela.activities.PaymentModeActivity;
import com.tcs.vikrela.adapters.PendingAdapter;
import com.tcs.vikrela.constants.NetworkConstants;
import com.tcs.vikrela.dto.PendingDTO;
import com.tcs.vikrela.global.AppController;
import com.tcs.vikrela.ui.Snackbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class RentFragment extends Fragment {

    @Bind(R.id.recyclerViewPending)
    RecyclerView recyclerViewPending;
    //private PendingAdapter adapter;
    private List<PendingDTO> data;
    MaterialDialog dialog;
    View parentView;
    SharedPreferences preferences;

    public RentFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        parentView = inflater.inflate(R.layout.fragment_rent, container, false);
        ButterKnife.bind(this, parentView);
        preferences = getActivity().getSharedPreferences("Vikrela", Context.MODE_PRIVATE);
        populate();
        return parentView;
    }

    private void populate() {
        /*dialog = new MaterialDialog.Builder(getContext())
                .title(getString(R.string.dialog_title))
                .content(getString(R.string.dialog_content))
                .progress(true, 0)
                .cancelable(false)
                .show();*/

        data = new ArrayList<>();
        StringRequest request = new StringRequest(Request.Method.POST, NetworkConstants.RENT_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("Response:::", response);
                //dialog.dismiss();
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    Log.i("Length::::", String.valueOf(jsonArray.length()));
                    for (int i = 0; i < jsonArray.length(); i++) {
                        PendingDTO dto = new PendingDTO();
                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                        dto.setName(i + 1 + ". " + jsonObject1.getString("name"));
                        dto.setAddress(jsonObject1.getString("business_place"));
                        dto.setAmount("Rs. " + jsonObject1.getString("amount"));
                        dto.setId(jsonObject1.getString("lisence"));
                        data.add(dto);
                    }
                    final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
                    recyclerViewPending.setLayoutManager(linearLayoutManager);
                    if (getActivity() != null){
                        recyclerViewPending.setAdapter(new PendingAdapter(getContext(), data, new PendingAdapter.OnItemClickListener() {
                            @Override
                            public void onItemClick(PendingDTO pendingDTO) {
                                Intent intent = new Intent(getContext(), PaymentModeActivity.class);
                                preferences.edit().putString("license_number", pendingDTO.getId()).commit();
                                startActivity(intent);
                                //Toast.makeText(getActivity(), pendingDTO.getId(), Toast.LENGTH_SHORT).show();
                            }
                        }));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Snackbar.show(getActivity(), getString(R.string.network_error));
                //dialog.dismiss();
                Log.e("Error:::", error.toString());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                String division = preferences.getString("division", null);
                params.put("division", division);
                return params;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(request);
        //adapter.notifyDataSetChanged();
        /*adapter.setOnLoadMoreListener(new PendingAdapter.OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                if (data.size() <= 20) {
                    data.add(null);
                    adapter.notifyItemInserted(data.size() - 1);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            data.remove(data.size() - 1);
                            adapter.notifyItemRemoved(data.size());

                            int index = data.size();
                            int end = index + 10;
                            for (int i = index; i < end; i++) {
                                getData();
                            }
                            adapter.notifyDataSetChanged();
                            adapter.setLoaded();
                        }
                    }, 3000);
                } else {

                }
            }
        });*/
    }
/*
    public List<PendingDTO> getData() {
        Log.d("data:::", data.toString());
        return data;

    }*/
}