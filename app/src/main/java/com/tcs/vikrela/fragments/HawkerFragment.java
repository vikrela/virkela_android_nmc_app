package com.tcs.vikrela.fragments;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.tcs.vikrela.R;
import com.tcs.vikrela.activities.HawkerInfoActivity;
import com.tcs.vikrela.activities.ScanQRCodeActivity;
import com.tcs.vikrela.ui.Snackbar;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class HawkerFragment extends Fragment {


    @Bind(R.id.etLicenseNumber)
    EditText etLicenseNumber;
    @Bind(R.id.btnEnter)
    Button btnEnter;
    @Bind(R.id.btnScanQR)
    ImageView btnScanQR;
    SharedPreferences preferences;

    @OnClick(R.id.btnScanQR)
    void scanQR() {
        startActivity(new Intent(getContext(), ScanQRCodeActivity.class));
    }

    @OnClick(R.id.btnEnter)
    void info() {
        if (etLicenseNumber.getText().toString().equals("")) {
            Snackbar.show(getActivity(), getString(R.string.empty_field_error));
            return;
        } else {
            preferences.edit().putString("has_qr_code", "no").commit();
            Intent intent = new Intent(getActivity(), HawkerInfoActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString("license", etLicenseNumber.getText().toString());
            intent.putExtras(bundle);
            startActivity(intent);
        }
    }


    public HawkerFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_hawker, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        ButterKnife.bind(this, view);
        preferences = getActivity().getSharedPreferences("Vikrela", Context.MODE_PRIVATE);
        etLicenseNumber.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        return view;
    }

    @Override
    public void onResume() {
        etLicenseNumber.setText("");
        super.onResume();
    }
}
