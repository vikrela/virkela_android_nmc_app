package com.tcs.vikrela.fragments;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.tcs.vikrela.R;
import com.tcs.vikrela.activities.InspectionActivity;
import com.tcs.vikrela.adapters.InspectionAdapter;
import com.tcs.vikrela.constants.NetworkConstants;
import com.tcs.vikrela.dto.InspectionDTO;
import com.tcs.vikrela.global.AppController;
import com.tcs.vikrela.ui.Snackbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class InspectionFragment extends Fragment {

    @Bind(R.id.recyclerViewInspection)
    RecyclerView recyclerViewInspection;
    //private InspectionAdapter adapter;
    private List<InspectionDTO> data;
    MaterialDialog dialog;
    SharedPreferences preferences;

    public InspectionFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View parentView = inflater.inflate(R.layout.fragment_inspection, container, false);
        ButterKnife.bind(this, parentView);
        preferences = getActivity().getSharedPreferences("Vikrela", Context.MODE_PRIVATE);
        populate();
        return parentView;
    }

    private void populate() {
        data = new ArrayList<>();
        if (data.size() != 0) {
            final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
            recyclerViewInspection.setLayoutManager(linearLayoutManager);
            if (getActivity() != null) {
                recyclerViewInspection.setAdapter(new InspectionAdapter(getActivity(), data, new InspectionAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(InspectionDTO inspectionDTO) {
                        Intent intent = new Intent(getContext(), InspectionActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("sent_from_server", "yes");
                        intent.putExtras(bundle);
                        preferences.edit().putString("license_number", inspectionDTO.getId()).commit();
                        startActivity(intent);
                        //Toast.makeText(getActivity(), pendingDTO.getId(), Toast.LENGTH_SHORT).show();
                    }
                }));
            }
        } else {
            dialog = new MaterialDialog.Builder(getContext())
                    .title(getString(R.string.dialog_title))
                    .content(getString(R.string.dialog_content))
                    .progress(true, 0)
                    .show();

            StringRequest request = new StringRequest(Request.Method.POST, NetworkConstants.RENT_URL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.d("Response:::", response);
                    dialog.dismiss();
                    try {
                        JSONArray jsonArray = new JSONArray(response);
                        Log.i("Length::::", String.valueOf(jsonArray.length()));
                        for (int i = 0; i < jsonArray.length(); i++) {
                            InspectionDTO dto = new InspectionDTO();
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            dto.setName(i + 1 + ". " + jsonObject1.getString("name"));
                            dto.setAddress(jsonObject1.getString("business_place"));
                            dto.setId(jsonObject1.getString("lisence"));
                            data.add(dto);
                        }
                        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
                        recyclerViewInspection.setLayoutManager(linearLayoutManager);
                        if (getActivity() != null) {
                            recyclerViewInspection.setAdapter(new InspectionAdapter(getActivity(), data, new InspectionAdapter.OnItemClickListener() {
                                @Override
                                public void onItemClick(InspectionDTO inspectionDTO) {
                                    Intent intent = new Intent(getContext(), InspectionActivity.class);
                                    Bundle bundle = new Bundle();
                                    bundle.putString("sent_from_server", "yes");
                                    intent.putExtras(bundle);
                                    preferences.edit().putString("license_number", inspectionDTO.getId()).commit();
                                    startActivity(intent);
                                    //Toast.makeText(getActivity(), pendingDTO.getId(), Toast.LENGTH_SHORT).show();
                                }
                            }));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Snackbar.show(getActivity(), getString(R.string.network_error));
                    dialog.dismiss();
                    Log.e("Error:::", error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    String division = preferences.getString("division", null);
                    params.put("division", division);
                    return params;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            AppController.getInstance().addToRequestQueue(request);
        }
/*
        adapter = new InspectionAdapter(getActivity(), getData(), new InspectionAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(InspectionDTO inspectionDTO) {
                //Toast.makeText(getActivity(), inspectionDTO.getName() + " Clicked", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getActivity(), InspectionActivity.class));
            }
        });
        recyclerViewInspection.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        recyclerViewInspection.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    public List<InspectionDTO> getData() {
        data = new ArrayList<>();
        String[] name = {getString(R.string.shantaram), getString(R.string.shubhwndu), getString(R.string.pratik), getString(R.string.harsh)};
        String[] address = {getString(R.string.cidco), getString(R.string.panchvati), getString(R.string.nasik_road), getString(R.string.nashik_east)};
        for (int i = 0; i < name.length; i++) {
            InspectionDTO dto = new InspectionDTO();
            dto.setName(name[i]);
            dto.setAddress(address[i]);
            data.add(dto);
        }
        return data;
    }
*/

    }
}
