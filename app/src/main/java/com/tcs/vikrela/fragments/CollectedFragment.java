package com.tcs.vikrela.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.tcs.vikrela.R;
import com.tcs.vikrela.adapters.CollectedAdapter;
import com.tcs.vikrela.dto.CollectedDTO;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class CollectedFragment extends Fragment {

    @Bind(R.id.recyclerViewCollected)
    RecyclerView recyclerViewCollected;
    private CollectedAdapter adapter;
    private List<CollectedDTO> data;

    public CollectedFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View parentView = inflater.inflate(R.layout.fragment_collected, container, false);
        ButterKnife.bind(this, parentView);
        populate();
        return parentView;
    }

    private void populate() {
        adapter = new CollectedAdapter(getActivity(), getData(), new CollectedAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(CollectedDTO collectedDTO) {
                Toast.makeText(getActivity(), collectedDTO.getName() + " Clicked", Toast.LENGTH_SHORT).show();
            }
        });
        recyclerViewCollected.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        recyclerViewCollected.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    public List<CollectedDTO> getData() {
        data = new ArrayList<>();
        String[] name = {getString(R.string.shantaram), getString(R.string.shubhwndu), getString(R.string.pratik), getString(R.string.harsh)};
        String[] address = {getString(R.string.cidco), getString(R.string.panchvati), getString(R.string.nasik_road), getString(R.string.nashik_east)};
        String[] amount = {"Rs. 20", "Rs. 30", "Rs. 60", "Rs. 45"};
        for (int i = 0; i < name.length; i++) {
            CollectedDTO dto = new CollectedDTO();
            dto.setName(name[i]);
            dto.setAddress(address[i]);
            dto.setAmount(amount[i]);
            data.add(dto);
        }
        return data;
    }

}
